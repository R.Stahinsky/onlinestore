package util;

import entity.CustomerEntity;
import entity.OrderDetailsEntity;
import entity.OrderEntity;
import entity.ProductCategoryEntity;
import entity.ProductEntity;
import java.util.List;
import org.junit.jupiter.api.Assertions;

public class AssertionsCaseForEntities {
  public static void assertCustomersFields(final CustomerEntity customer,
      final CustomerEntity savedCustomer) {
    Assertions.assertNotNull(savedCustomer);
    Assertions.assertNotNull(savedCustomer.getId());
    Assertions.assertEquals(customer.getEmail(), savedCustomer.getEmail());
    Assertions.assertEquals(customer.getGender(), savedCustomer.getGender());
    Assertions.assertEquals(customer.getAddress(), savedCustomer.getAddress());
    Assertions.assertEquals(customer.getLastName(), savedCustomer.getLastName());
    Assertions.assertEquals(customer.getFirstName(), savedCustomer.getFirstName());
    Assertions.assertEquals(customer.getBirthDate(), savedCustomer.getBirthDate());
    Assertions.assertEquals(customer.getCreatedAt(), savedCustomer.getCreatedAt());
    Assertions.assertEquals(customer.getUpdatedAt(), savedCustomer.getUpdatedAt());
  }

  public static void assertCustomersListFields(final CustomerEntity savedCustomer,
      final List<CustomerEntity> foundCustomers) {
    Assertions.assertEquals(1, foundCustomers.size());
    assertCustomersFields(savedCustomer, foundCustomers.stream().findFirst().get());
  }

  public static void assertOrderDetailsFields(final OrderDetailsEntity orderDetails,
      final OrderDetailsEntity savedOrderDetails) {
    Assertions.assertNotNull(savedOrderDetails);
    Assertions.assertNotNull(savedOrderDetails.getId());
    Assertions.assertNotNull(savedOrderDetails.getOrder().getId());
    Assertions.assertEquals(orderDetails.getCount(), savedOrderDetails.getCount());
    Assertions.
        assertEquals(orderDetails.getOrder().getId(),
            savedOrderDetails.getOrder().getId());
    Assertions.
        assertEquals(orderDetails.getProduct().getId(),
            savedOrderDetails.getProduct().getId());
  }

  public static void assertOrderDetailsListFields(final OrderDetailsEntity savedOrderDetails,
      final List<OrderDetailsEntity> foundOrderDetails) {
    Assertions.assertEquals(1, foundOrderDetails.size());
    assertOrderDetailsFields(savedOrderDetails, foundOrderDetails.stream().findFirst().get());
  }


  public static void assertOrdersFields(final OrderEntity order, final OrderEntity savedOrder) {
    Assertions.assertNotNull(savedOrder);
    Assertions.assertNotNull(savedOrder.getId());
    Assertions.assertEquals(order.getPrice(), savedOrder.getPrice());
    Assertions.assertEquals(order.getStatus(), savedOrder.getStatus());
    Assertions.assertEquals(order.getAddress(), savedOrder.getAddress());
    Assertions.assertEquals(order.getOrderDate(), savedOrder.getOrderDate());
    Assertions.assertEquals(order.getCreatedAt(), savedOrder.getCreatedAt());
    Assertions.assertEquals(order.getUpdatedAt(), savedOrder.getUpdatedAt());
    Assertions.assertEquals(order.getDeletedAt(), savedOrder.getDeletedAt());
    Assertions.assertEquals(order.getOrderNumber(), savedOrder.getOrderNumber());
    Assertions.assertEquals(order.getCustomer().getId(), savedOrder.getCustomer().getId());
  }

  public static void assertOrderListFields(final OrderEntity savedOrder,
      final List<OrderEntity> foundOrders) {
    Assertions.assertEquals(1, foundOrders.size());
    assertOrdersFields(savedOrder, foundOrders.stream().findFirst().get());
  }

  public static void assertProductCategoriesFields(final ProductCategoryEntity productCategory,
      final ProductCategoryEntity savedProductCategory) {
    Assertions.assertNotNull(savedProductCategory);
    Assertions.assertNotNull(savedProductCategory.getId());
    Assertions.assertNotNull(savedProductCategory.getName());
    Assertions.assertEquals(productCategory.getName(), savedProductCategory.getName());
  }

  public static void assertProductsFields(final ProductEntity product,
      final ProductEntity savedProduct) {
    Assertions.assertNotNull(savedProduct);
    Assertions.assertNotNull(savedProduct.getId());
    Assertions.assertNotNull(savedProduct.getProductCategory());
    Assertions.assertEquals(product.getName(), savedProduct.getName());
    Assertions.assertEquals(product.getPrice(), savedProduct.getPrice());
    Assertions.assertEquals(product.getCreatedAt(), savedProduct.getCreatedAt());
    Assertions.assertEquals(product.getUpdatedAt(), savedProduct.getUpdatedAt());
    Assertions.assertEquals(product.getDeletedAt(), savedProduct.getDeletedAt());
    Assertions.assertEquals(product.getDescription(), savedProduct.getDescription());
    Assertions
        .assertEquals(product.getProductCategory().getId(),
            savedProduct.getProductCategory().getId());
  }

  public static void assertProductsListFields(final ProductEntity savedProduct,
      final List<ProductEntity> foundProducts) {
    Assertions.assertEquals(1, foundProducts.size());
    assertProductsFields(savedProduct, foundProducts.stream().findFirst().get());
  }
}
