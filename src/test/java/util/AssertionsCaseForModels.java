package util;

import entity.CustomerEntity;
import entity.OrderDetailsEntity;
import entity.OrderEntity;
import java.util.List;
import model.CustomerModel;
import model.OrderModel;
import model.ProductModel;
import org.junit.jupiter.api.Assertions;

public class AssertionsCaseForModels {

  public static void assertOrderModelsList(final OrderEntity order,
      final List<OrderModel> foundOrderModels) {
    Assertions.assertNotNull(foundOrderModels);
    Assertions.assertEquals(1, foundOrderModels.size());
    Assertions.
        assertNotNull(foundOrderModels.stream().findFirst().get().getId());
    Assertions.
        assertEquals(order.getId(),
            foundOrderModels.stream().findFirst().get().getId());
    Assertions.
        assertEquals(order.getOrderNumber(),
            foundOrderModels.stream().findFirst().get().getOrderNumber());
    Assertions.
        assertEquals(order.getStatus(),
            foundOrderModels.stream().findFirst().get().getStatus());
    Assertions.
        assertEquals(order.getCreatedAt(),
            foundOrderModels.stream().findFirst().get().getCreatedAt());
    Assertions.
        assertEquals(order.getDeletedAt(),
            foundOrderModels.stream().findFirst().get().getDeletedAt());
    Assertions.
        assertEquals(order.getUpdatedAt(),
            foundOrderModels.stream().findFirst().get().getUpdatedAt());
  }

  public static void assertCustomerModelFields(final CustomerEntity customer,
      final CustomerModel foundCustomerModel) {
    Assertions.assertNotNull(foundCustomerModel);
    Assertions.assertNotNull(foundCustomerModel.getId());
    Assertions.assertEquals(customer.getEmail(), foundCustomerModel.getEmail());
    Assertions.assertEquals(customer.getGender(), foundCustomerModel.getGender());
    Assertions.assertEquals(customer.getGender(), foundCustomerModel.getGender());
    Assertions
        .assertEquals(customer.getAddress(), foundCustomerModel.getAddress());
    Assertions
        .assertEquals(customer.getLastName(), foundCustomerModel.getLastName());
    Assertions
        .assertEquals(customer.getBirthDate(), foundCustomerModel.getBirthDate());
    Assertions
        .assertEquals(customer.getCreatedAt(), foundCustomerModel.getCreatedAt());
    Assertions
        .assertEquals(customer.getUpdatedAt(), foundCustomerModel.getUpdatedAt());
    Assertions
        .assertEquals(customer.getDeletedAt(), foundCustomerModel.getDeletedAt());
    Assertions
        .assertEquals(customer.getFirstName(), foundCustomerModel.getFirstName());
  }

  public static void assertProductModelFields(final OrderDetailsEntity orderDetails,
      final ProductModel foundProductModel) {
    Assertions.assertNotNull(foundProductModel);
    Assertions.assertEquals(orderDetails.getCount(), foundProductModel.getCount());
    Assertions.assertEquals(orderDetails.getProduct().getId(), foundProductModel.getId());
    Assertions.assertEquals(orderDetails.getProduct().getPrice(), foundProductModel.getPrice());
    Assertions.assertEquals(orderDetails.getProduct().getUpdatedAt(),
        foundProductModel.getUpdatedAt());
    Assertions.assertEquals(orderDetails.getProduct().getDeletedAt(),
        foundProductModel.getDeletedAt());
    Assertions.assertEquals(orderDetails.getProduct().getDescription(),
        foundProductModel.getDescription());
    Assertions
        .assertEquals(orderDetails.getProduct().getName(), foundProductModel.getName());
    Assertions.
        assertEquals(orderDetails.getProduct().getCreatedAt(), foundProductModel.getCreatedAt());
  }
}
