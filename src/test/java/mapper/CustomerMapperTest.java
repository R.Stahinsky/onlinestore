package mapper;

import static mapper.CustomerMapper.mapToCustomerModel;
import static mapper.util.EntityGeneratorUtil.generateCustomer;

import entity.CustomerEntity;
import model.CustomerModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CustomerMapperTest {

  @Test
  public void mapToModel() {
    //given
    final CustomerEntity customer = generateCustomer();

    //when
    final CustomerModel customerModel = mapToCustomerModel(customer);

    //then
    Assertions.assertNotNull(customerModel);
    Assertions.assertEquals(customer.getId(), customerModel.getId());
    Assertions.assertEquals(customer.getEmail(), customerModel.getEmail());
    Assertions.assertEquals(customer.getAddress(), customerModel.getAddress());
    Assertions.assertEquals(customer.getGender(), customerModel.getGender());
    Assertions.assertEquals(customer.getOrders(), customerModel.getOrders());
    Assertions.assertEquals(customer.getLastName(), customerModel.getLastName());
    Assertions.assertEquals(customer.getBirthDate(), customerModel.getBirthDate());
    Assertions.assertEquals(customer.getFirstName(), customerModel.getFirstName());
    Assertions.assertEquals(customer.getCreatedAt(), customerModel.getCreatedAt());
    Assertions.assertEquals(customer.getUpdatedAt(), customerModel.getUpdatedAt());
    Assertions.assertEquals(customer.getDeletedAt(), customerModel.getDeletedAt());
  }
}
