package mapper;

import static mapper.ProductCategory.mapToProductCategoryModel;
import static mapper.util.EntityGeneratorUtil.generateProductCategory;

import entity.ProductCategoryEntity;
import model.ProductCategoryModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProductCategoryMapperTest {

  @Test
  public void mapToModel() {
    //given
    final ProductCategoryEntity productCategory = generateProductCategory();

    //when
    final ProductCategoryModel productCategoryModel = mapToProductCategoryModel(productCategory);

    //then
    Assertions.assertNotNull(productCategory);
    Assertions.assertEquals(productCategory.getId(), productCategoryModel.getId());
    Assertions.assertEquals(productCategory.getName(), productCategoryModel.getName());
  }
}
