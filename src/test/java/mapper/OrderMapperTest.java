package mapper;

import static mapper.OrderMapper.mapToOrderModel;
import static mapper.util.EntityCreatorUtil.createOrderWithEntities;

import entity.OrderEntity;
import model.OrderModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class OrderMapperTest {

  @Test
  public void mapToModel() {
    //given
    final OrderEntity order = createOrderWithEntities();

    //when
    final OrderModel orderModel = mapToOrderModel(order, order.getOrderDetails());

    //then
    assertOrderModelFields(order, orderModel);
  }

  @Test
  public void mapToModelFromList() {
    //given
    final OrderEntity order = createOrderWithEntities();

    //when
    final OrderModel orderModel = mapToOrderModel(order, order.getOrderDetails());

    //then
    assertOrderModelFields(order, orderModel);
  }

  private void assertOrderModelFields(final OrderEntity order, final OrderModel orderModel){
    Assertions.assertNotNull(orderModel);
    Assertions.assertEquals(order.getId(), orderModel.getId());
    Assertions.assertEquals(order.getStatus(), orderModel.getStatus());
    Assertions.assertEquals(order.getOrderDate(), orderModel.getDate());
    Assertions.assertEquals(order.getAddress(), orderModel.getAddress());
    Assertions.assertEquals(order.getCreatedAt(), orderModel.getCreatedAt());
    Assertions.assertEquals(order.getUpdatedAt(), orderModel.getUpdatedAt());
    Assertions.assertEquals(order.getDeletedAt(), orderModel.getDeletedAt());
    Assertions.assertEquals(order.getOrderNumber(), orderModel.getOrderNumber());
    Assertions.assertEquals(order.getCustomer().getId(), orderModel.getCustomerId());
    Assertions.
        assertEquals(order.getOrderDetails().stream().findFirst().get().getOrder().getId(),
            orderModel.getOrderDetails().stream().findFirst().get().getOrderId());
  }
}
