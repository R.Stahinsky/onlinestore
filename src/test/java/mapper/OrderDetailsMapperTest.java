package mapper;

import static mapper.OrderDetailsMapper.mapToOrderDetailsModel;
import static mapper.util.EntityCreatorUtil.createOrderDetailsWithEntities;

import entity.OrderDetailsEntity;
import java.util.List;
import model.OrderDetailsModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class OrderDetailsMapperTest {

  @Test
  public void mapToModel() {
    //given
    final OrderDetailsEntity orderDetails = createOrderDetailsWithEntities();

    //when
    final OrderDetailsModel orderDetailsModel = mapToOrderDetailsModel(orderDetails);

    //then
    Assertions.assertNotNull(orderDetails);
    Assertions.assertEquals(orderDetails.getId(), orderDetailsModel.getId());
    Assertions.assertEquals(orderDetails.getCount(), orderDetailsModel.getCount());
    Assertions.assertEquals(orderDetails.getProduct().getId(), orderDetailsModel.getProductId());
    Assertions.assertEquals(orderDetails.getOrder().getId(), orderDetailsModel.getOrderId());
  }

  @Test
  public void mapToModelFromList() {
    //given
    final OrderDetailsEntity orderDetails = createOrderDetailsWithEntities();

    //when
    final List<OrderDetailsModel> orderDetailsModel = mapToOrderDetailsModel(List.of(orderDetails));

    //then
    Assertions.assertNotNull(orderDetails);
    Assertions
        .assertEquals(orderDetails.getId(), orderDetailsModel.stream().findFirst().get().getId());
    Assertions.
        assertEquals(orderDetails.getCount(),
            orderDetailsModel.stream().findFirst().get().getCount());
    Assertions.
        assertEquals(orderDetails.getOrder().getId(),
            orderDetailsModel.stream().findFirst().get().getOrderId());
    Assertions.
        assertEquals(orderDetails.getProduct().getId(),
            orderDetailsModel.stream().findFirst().get().getProductId());
  }
}
