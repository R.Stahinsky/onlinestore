package mapper.util;

import static mapper.util.EntityGeneratorUtil.generateCustomer;
import static mapper.util.EntityGeneratorUtil.generateOrder;
import static mapper.util.EntityGeneratorUtil.generateOrderDetails;
import static mapper.util.EntityGeneratorUtil.generateProduct;
import static mapper.util.EntityGeneratorUtil.generateProductCategory;

import entity.CustomerEntity;
import entity.OrderDetailsEntity;
import entity.OrderEntity;
import entity.ProductCategoryEntity;
import entity.ProductEntity;
import java.util.List;
import java.util.UUID;

public class EntityCreatorUtil {

  public static OrderDetailsEntity createOrderDetailsWithEntities() {
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();
    final CustomerEntity generatedCustomer = generateCustomer();
    final ProductEntity generatedProduct = generateProduct();
    final OrderEntity generatedOrder = generateOrder();
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();

    generatedProduct.setProductCategory(generatedProductCategory);
    generatedOrder.setCustomer(generatedCustomer);
    generatedOrderDetails.setProduct(generatedProduct);
    generatedOrderDetails.setOrder(generatedOrder);

    return generatedOrderDetails;
  }

  public static OrderEntity createOrderWithEntities() {
    final OrderEntity generatedOrder = generateOrder();
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    final ProductEntity generatedProduct = generateProduct();

    generatedOrderDetails.setProduct(generatedProduct);
    generatedOrder.setOrderDetails(List.of(generatedOrderDetails));

    return generatedOrder;
  }

  public static ProductEntity createProductWithEntities(){
    final ProductEntity generatedProduct = generateProduct();
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();

    generatedProductCategory.setId(UUID.randomUUID());
    generatedProduct.setId(UUID.randomUUID());
    generatedProduct.setProductCategory(generatedProductCategory);

    return generatedProduct;
  }
}
