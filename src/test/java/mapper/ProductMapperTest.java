package mapper;

import static mapper.ProductMapper.mapToProductModel;
import static mapper.util.EntityCreatorUtil.createProductWithEntities;
import static mapper.util.EntityGeneratorUtil.generateOrderDetails;

import entity.OrderDetailsEntity;
import entity.ProductEntity;
import model.ProductModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProductMapperTest {

  @Test
  public void mapToModel() {
    //given
    final ProductEntity product = createProductWithEntities();
    final OrderDetailsEntity orderDetailsEntity = generateOrderDetails();

    //when
    final ProductModel productModel = mapToProductModel(product, orderDetailsEntity.getCount());

    //then
    Assertions.assertNotNull(productModel);
    Assertions.assertEquals(product.getId(), productModel.getId());
    Assertions.assertEquals(product.getName(), productModel.getName());
    Assertions.assertEquals(product.getPrice(), productModel.getPrice());
    Assertions.assertEquals(product.getCreatedAt(), productModel.getCreatedAt());
    Assertions.assertEquals(product.getUpdatedAt(), productModel.getUpdatedAt());
    Assertions.assertEquals(product.getDeletedAt(), productModel.getDeletedAt());
    Assertions.assertEquals(orderDetailsEntity.getCount(), productModel.getCount());
    Assertions.assertEquals(product.getDescription(), productModel.getDescription());
    Assertions.assertEquals(
        product.getProductCategory().getId(), productModel.getProductCategory().getId());
  }
}
