package service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static service.util.EntityCreatorUtil.createOrderDetailsWithEntities;
import static service.util.EntityCreatorUtil.createOrderWithEntities;
import static service.util.EntityGeneratorUtil.generateCustomer;
import static service.util.EntityGeneratorUtil.generateOrder;
import static util.AssertionsCaseForModels.assertCustomerModelFields;
import static util.AssertionsCaseForModels.assertOrderModelsList;
import static util.AssertionsCaseForModels.assertProductModelFields;

import entity.CustomerEntity;
import entity.OrderDetailsEntity;
import entity.OrderEntity;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import model.CustomerModel;
import model.OrderModel;
import model.ProductModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.CustomerRepository;
import repository.OrderDetailsRepository;
import repository.OrderRepository;
import repository.ProductRepository;
import service.impl.StatisticServiceImpl;

@ExtendWith(MockitoExtension.class)
public class StatisticServiceTest {

  @Mock
  private CustomerRepository customerRepository;

  @Mock
  private OrderRepository orderRepository;

  @Mock
  private OrderDetailsRepository orderDetailsRepository;

  @Mock
  private ProductRepository productRepository;

  private StatisticService statisticService;

  @BeforeEach
  public void setUp() {
    statisticService = new StatisticServiceImpl(customerRepository, orderRepository,
        orderDetailsRepository, productRepository);
  }

  @Test
  public void getOrdersWithinLastTwoWeeks_happyPath() {
    //given
    final OrderEntity generatedOrder = createOrderWithEntities();
    when(orderRepository.findAllBeforeDate(any()))
        .thenReturn(List.of(generatedOrder));
    when(orderDetailsRepository.findAllByOrderId(generatedOrder.getId()))
        .thenReturn(generatedOrder.getOrderDetails());
    //when
    final List<OrderModel> foundOrdersWithinLastTwoWeeks =
        statisticService.getOrdersWithinLastTwoWeeks();

    //then
    assertOrderModelsList(generatedOrder, foundOrdersWithinLastTwoWeeks);
  }

  @Test
  public void getCustomerWithBiggestHistoryLastMonth_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();
    final OrderEntity orderToSave = generateOrder();
    when(orderRepository.findAllBeforeDate(any()))
        .thenReturn(List.of(orderToSave));
    when(customerRepository.findById(orderToSave.getCustomer().getId()))
        .thenReturn(Optional.of(generatedCustomer));

    //when
    final CustomerModel foundCustomerWithBiggestHistory = statisticService.
        getCustomerWithBiggestHistoryLastMonth();

    //then
    assertCustomerModelFields(generatedCustomer, foundCustomerWithBiggestHistory);
  }

  @Test
  public void getMostPopularProductBeforeDate_happyPath() {
    //given
    final Instant date = Instant.parse("2022-10-08T00:00:00.00000Z");
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(productRepository.findAllBeforeDate(date))
        .thenReturn(List.of(generatedOrderDetails.getProduct()));
    when(orderDetailsRepository.countByProductId(generatedOrderDetails.getProduct().getId()))
        .thenReturn(generatedOrderDetails.getCount());
    when(productRepository.findById(generatedOrderDetails.getProduct().getId()))
        .thenReturn(Optional.of(generatedOrderDetails.getProduct()));

    //when
    final ProductModel foundPopularProduct = statisticService.getMostPopularProductBeforeDate(date);

    //then
    assertProductModelFields(generatedOrderDetails, foundPopularProduct);
  }

  @Test
  public void getCountOfOrdersByProductIdAndDate_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    final Instant date = Instant.parse("2021-10-08T00:00:00.00000Z");
    when(productRepository.findAllBeforeDate(date))
        .thenReturn(List.of(generatedOrderDetails.getProduct()));
    when(orderDetailsRepository.countByProductId(generatedOrderDetails.getProduct().getId()))
        .thenReturn(generatedOrderDetails.getCount());

    //when
    final Integer count = statisticService
        .getCountOfOrdersByProductIdAndDate(generatedOrderDetails.getProduct().getId(), date);

    //then
    Assertions.assertNotNull(count);
    Assertions.assertEquals(generatedOrderDetails.getCount(), count);
  }

  @Test
  public void getAveragePriceInLastMonth_happyPath() {
    //given
    final OrderEntity generatedOrder = createOrderWithEntities();
    when(orderRepository.countAveragePriceBeforeDate(any()))
        .thenReturn(generatedOrder.getPrice());

    //when
    final Double averagePrice = statisticService.getAveragePriceInLastMonth();

    //then
    Assertions.assertNotNull(averagePrice);
    Assertions.assertEquals(generatedOrder.getPrice(), averagePrice);
  }
}
