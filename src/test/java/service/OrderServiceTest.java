package service;

import static mapper.OrderMapper.mapToOrderModel;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static service.util.EntityCreatorUtil.createOrderWithEntities;
import static service.util.EntityGeneratorUtil.generateOrder;
import static util.AssertionsCaseForEntities.assertOrderListFields;
import static util.AssertionsCaseForEntities.assertOrdersFields;

import entity.OrderEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import repository.OrderRepository;
import service.impl.OrderServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class OrderServiceTest {

  @Mock
  private OrderRepository orderRepository;

  private OrderService orderService;

  @BeforeEach
  public void setUp() {
    orderService = new OrderServiceImpl(orderRepository);
  }

  @Test
  public void getById_happyPath() {
    //given
    final OrderEntity generatedOrder = createOrderWithEntities();
    when(orderRepository.findById(generatedOrder.getId())).thenReturn(Optional.of(generatedOrder));

    //when
    final OrderEntity foundOrder = orderService.getById(generatedOrder.getId());

    //then
    assertOrdersFields(generatedOrder, foundOrder);
  }

  @Test
  public void getAll_happyPath() {
    //given
    final OrderEntity generatedOrder = createOrderWithEntities();
    when(orderRepository.findAll()).thenReturn(List.of(generatedOrder));

    //when
    final List<OrderEntity> foundOrders = orderService.getAll();

    //then
    assertOrderListFields(generatedOrder, foundOrders);
  }

  @Test
  public void create_happyPath() {
    //given
    final OrderEntity generatedOrder = createOrderWithEntities();
    when(orderRepository.findById(generatedOrder.getId())).thenReturn(Optional.of(generatedOrder));
    when(orderRepository.create(generatedOrder)).thenReturn(generatedOrder.getId());

    //when
    final OrderEntity savedOrder = orderService.create(generatedOrder);

    //then
    assertOrdersFields(generatedOrder, savedOrder);
  }

  @Test
  public void updateById_happyPath() {
    //given
    final OrderEntity generatedOrder = createOrderWithEntities();
    when(orderRepository.findById(generatedOrder.getId())).thenReturn(Optional.of(generatedOrder));
    generatedOrder.setOrderNumber(17);
    when(orderRepository.update(generatedOrder)).thenReturn(Optional.of(generatedOrder));

    //when
    final OrderEntity updatedOrder = orderService.updateById(generatedOrder.getId(),
        mapToOrderModel(generatedOrder, generatedOrder.getOrderDetails()));

    //then
    Assertions.assertEquals(17, updatedOrder.getOrderNumber());
    assertOrdersFields(generatedOrder, updatedOrder);
    Assertions.assertEquals(generatedOrder.getId(), updatedOrder.getId());
  }

  @Test
  public void getAllByCustomerId() {
    //given
    final OrderEntity generatedOrder = createOrderWithEntities();
    when(orderRepository.findAllByCustomerId(generatedOrder.getCustomer().getId()))
        .thenReturn(List.of(generatedOrder));

    //when
    final List<OrderEntity> foundOrders =
        orderService.getAllByCustomerId(generatedOrder.getCustomer().getId());

    //then
    assertOrderListFields(generatedOrder, foundOrders);
  }

  @Test
  public void getById_whenNotFound() {
    //given
    final UUID notExistingId = UUID.randomUUID();
    when(orderRepository.findById(notExistingId))
        .thenThrow(new EntityNotFoundException(OrderEntity.class.getName(), notExistingId));

    //when
    final Exception exception = Assertions
        .assertThrows(EntityNotFoundException.class, () -> orderService.getById(notExistingId));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
  }

  @Test
  public void updateById_whenNotFound() {
    //given
    final UUID notExisting = UUID.randomUUID();
    final OrderEntity generatedOrder = createOrderWithEntities();
    when(orderRepository.findById(notExisting))
        .thenThrow(new EntityNotFoundException(OrderEntity.class.getName(), notExisting));
    when(orderRepository.update(generatedOrder))
        .thenThrow(new EntityNotUpdatedException(OrderEntity.class.getName(), notExisting));

    //when
    final Exception exception = Assertions
        .assertThrows(EntityNotFoundException.class, () -> orderService
            .updateById(notExisting, mapToOrderModel(generatedOrder, generatedOrder.getOrderDetails())));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
  }

  @Test
  public void deleteById_whenNotFound() {
    //given
    final UUID notExisting = UUID.randomUUID();
    final OrderEntity generatedOrder = generateOrder();
    generatedOrder.setId(notExisting);
    when(orderRepository.findById(notExisting))
        .thenThrow(new EntityNotFoundException(OrderEntity.class.getName(), notExisting));
    doNothing().when(orderRepository).delete(generatedOrder);

    //when
    final Exception exception = Assertions
        .assertThrows(EntityNotFoundException.class, () -> orderService.deleteById(notExisting));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
  }
}
