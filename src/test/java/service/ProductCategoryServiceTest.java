package service;

import static mapper.ProductCategory.mapToProductCategoryModel;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static service.util.EntityGeneratorUtil.generateProductCategory;
import static util.AssertionsCaseForEntities.assertProductCategoriesFields;

import entity.ProductCategoryEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import repository.ProductCategoryRepository;
import service.impl.ProductCategoryServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ProductCategoryServiceTest {

  @Mock
  private ProductCategoryRepository productCategoryRepository;

  private ProductCategoryService productCategoryService;

  @BeforeEach
  public void setUp() {
    productCategoryService = new ProductCategoryServiceImpl(productCategoryRepository);
  }

  @Test
  public void getById_happyPath() {
    //given
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();
    when(productCategoryRepository.findById(generatedProductCategory.getId()))
        .thenReturn(Optional.of(generatedProductCategory));

    //when
    final ProductCategoryEntity foundProductCategory =
        productCategoryService.getById(generatedProductCategory.getId());

    //then
    assertProductCategoriesFields(generatedProductCategory, foundProductCategory);
  }

  @Test
  public void getAll_happyPath() {
    //given
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();
    when(productCategoryRepository.findAll()).thenReturn(List.of(generatedProductCategory));

    //when
    final List<ProductCategoryEntity> foundProductCategories = productCategoryService.getAll();

    //then
    Assertions.assertNotNull(foundProductCategories);
    Assertions.assertEquals(1, foundProductCategories.size());
    Assertions.
        assertEquals(generatedProductCategory.getId(),
            foundProductCategories.stream().findFirst().get().getId());
    Assertions.
        assertEquals(generatedProductCategory.getName(),
            foundProductCategories.stream().findFirst().get().getName());
  }

  @Test
  public void create_happyPath() {
    //given
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();
    when(productCategoryRepository.create(generatedProductCategory))
        .thenReturn(generatedProductCategory.getId());
    when(productCategoryRepository.findById(generatedProductCategory.getId()))
        .thenReturn(Optional.of(generatedProductCategory));

    //when
    final ProductCategoryEntity savedProductCategory =
        productCategoryService.create(generatedProductCategory);

    //then
    assertProductCategoriesFields(generatedProductCategory, savedProductCategory);
  }

  @Test
  public void updateById_happyPath() {
    //given
    final ProductCategoryEntity generateProductCategory = generateProductCategory();
    when(productCategoryRepository.findById(generateProductCategory.getId()))
        .thenReturn(Optional.of(generateProductCategory));
    generateProductCategory.setName("Beer");
    when(productCategoryRepository.update(generateProductCategory))
        .thenReturn(Optional.of(generateProductCategory));

    //when
    final ProductCategoryEntity updatedProductCategory =
        productCategoryService.updateById(generateProductCategory.getId(),
            mapToProductCategoryModel(generateProductCategory));

    //then
    Assertions.assertEquals("Beer", updatedProductCategory.getName());
    assertProductCategoriesFields(generateProductCategory, updatedProductCategory);
    Assertions.assertEquals(generateProductCategory.getId(), updatedProductCategory.getId());
  }

  @Test
  public void deleteById_happyPath() {
    //given
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();
    when(productCategoryRepository.findById(generatedProductCategory.getId()))
        .thenReturn(Optional.of(generatedProductCategory));
    doNothing().when(productCategoryRepository).delete(generatedProductCategory);

    //when
    productCategoryService.deleteById(generatedProductCategory.getId());

    //then
    verify(productCategoryRepository, times(1));
    Assertions.assertThrows(NullPointerException.class,
        () -> productCategoryService.getById(generatedProductCategory.getId()));
  }

  @Test
  public void getById_whenNotFound() {
    //given
    final UUID notExisting = UUID.randomUUID();
    when(productCategoryRepository.findById(notExisting))
        .thenThrow(new EntityNotFoundException(ProductCategoryEntity.class.getName(), notExisting));

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> productCategoryService.getById(notExisting));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
  }

  @Test
  public void updateById_whenNotFound() {
    //given
    final UUID notExisting = UUID.randomUUID();
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();
    when(productCategoryRepository.findById(notExisting))
        .thenThrow(new EntityNotFoundException(ProductCategoryEntity.class.getName(), notExisting));
    when(productCategoryRepository.update(generatedProductCategory))
        .thenThrow(
            new EntityNotUpdatedException(ProductCategoryEntity.class.getName(), notExisting));

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> productCategoryService
            .updateById(notExisting, mapToProductCategoryModel(generatedProductCategory)));
    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
  }

  @Test
  public void deleteById_whenNotFound() {
    //given
    final UUID notExisting = UUID.randomUUID();
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();
    generatedProductCategory.setId(notExisting);
    when(productCategoryRepository.findById(notExisting))
        .thenThrow(new EntityNotFoundException(ProductCategoryEntity.class.getName(), notExisting));
    doNothing().when(productCategoryRepository).delete(generatedProductCategory);

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> productCategoryService.deleteById(notExisting));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
  }
}
