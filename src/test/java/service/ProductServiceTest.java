package service;

import static mapper.ProductMapper.mapToProductModel;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static service.util.EntityCreatorUtil.createProductWithEntities;
import static service.util.EntityGeneratorUtil.generateProduct;
import static util.AssertionsCaseForEntities.assertProductsFields;
import static util.AssertionsCaseForEntities.assertProductsListFields;

import entity.ProductEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import repository.OrderDetailsRepository;
import repository.ProductRepository;
import service.impl.OrderDetailsServiceImpl;
import service.impl.ProductServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ProductServiceTest {

  @Mock
  private OrderDetailsRepository orderDetailsRepository;

  @Mock
  private ProductRepository productRepository;

  private OrderDetailsService orderDetailsService;
  private ProductService productService;

  @BeforeEach
  public void setUp() {
    orderDetailsService = new OrderDetailsServiceImpl(orderDetailsRepository);
    productService = new ProductServiceImpl(productRepository, orderDetailsService);
  }

  @Test
  public void getById_happyPath() {
    //given
    final ProductEntity generatedProduct = createProductWithEntities();
    when(productRepository.findById(generatedProduct.getId()))
        .thenReturn(Optional.of(generatedProduct));

    //when
    final ProductEntity foundProduct = productService.getById(generatedProduct.getId());

    //then
    assertProductsFields(generatedProduct, foundProduct);
  }

  @Test
  public void getAll_happyPath() {
    //given
    final ProductEntity generatedProduct = createProductWithEntities();
    when(productRepository.findAll()).thenReturn(Arrays.asList(generatedProduct));

    //when
    final List<ProductEntity> foundProducts = productService.getAll();

    //then
    assertProductsListFields(generatedProduct, foundProducts);
  }

  @Test
  public void create_happyPath() {
    //given
    final ProductEntity generatedProduct = createProductWithEntities();
    when(productRepository.create(generatedProduct)).thenReturn(generatedProduct.getId());
    when(productRepository.findById(generatedProduct.getId()))
        .thenReturn(Optional.of(generatedProduct));

    //when
    final ProductEntity savedProduct = productService.create(generatedProduct);

    //then
    assertProductsFields(generatedProduct, savedProduct);
  }

  @Test
  public void updateById_happyPath() {
    //given
    final ProductEntity generatedProduct = createProductWithEntities();
    when(productRepository.findById(generatedProduct.getId()))
        .thenReturn(Optional.of(generatedProduct));
    generatedProduct.setPrice(4.5);
    when(productRepository.update(generatedProduct)).thenReturn(Optional.of(generatedProduct));

    //when
    final ProductEntity updatedProduct = productService.updateById(generatedProduct.getId(),
        mapToProductModel(generatedProduct, 4));

    //then
    assertProductsFields(generatedProduct, updatedProduct);
    Assertions.assertEquals(4.5, updatedProduct.getPrice());
    Assertions.assertEquals(generatedProduct.getId(), updatedProduct.getId());
  }

  @Test
  public void deleteById_happyPath() {
    //given
    final ProductEntity generatedProduct = createProductWithEntities();
    generatedProduct.setDeletedAt(Instant.now());
    when(productRepository.findById(generatedProduct.getId()))
        .thenReturn(Optional.of(generatedProduct));
    doNothing().when(productRepository).delete(generatedProduct);

    //when
    productService.deleteById(generatedProduct.getId());

    //then
    verify(productRepository, times(1));
    Assertions.assertThrows(NullPointerException.class,
        () -> productService.getById(generatedProduct.getId()));
  }

  @Test
  public void getById_whenNotFound() {
    //given
    final UUID notExistingId = UUID.randomUUID();
    when(productRepository.findById(notExistingId))
        .thenThrow(new EntityNotFoundException(ProductEntity.class.getName(), notExistingId));

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> productService.getById(notExistingId));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
  }

  @Test
  public void updateById_whenNotFound() {
    //given
    final UUID notExisting = UUID.randomUUID();
    final ProductEntity generatedProduct = generateProduct();
    when(productRepository.findById(notExisting))
        .thenThrow(new EntityNotFoundException(ProductEntity.class.getName(), notExisting));
    when(productRepository.update(generatedProduct))
        .thenThrow(new EntityNotUpdatedException(ProductEntity.class.getName(), notExisting));

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> productService
            .updateById(notExisting, mapToProductModel(generatedProduct, 2)));
    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
  }

  @Test
  public void deleteById_whenNotFound() {
    //given
    final UUID notExisting = UUID.randomUUID();
    final ProductEntity generatedProduct = generateProduct();
    generatedProduct.setId(notExisting);
    when(productRepository.findById(notExisting))
        .thenThrow(new EntityNotFoundException(ProductEntity.class.getName(), notExisting));
    doNothing().when(productRepository).delete(generatedProduct);

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> productService.deleteById(notExisting));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
  }
}
