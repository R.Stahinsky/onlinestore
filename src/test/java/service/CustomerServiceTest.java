package service;

import static mapper.CustomerMapper.mapToCustomerModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static service.util.EntityGeneratorUtil.generateCustomer;
import static util.AssertionsCaseForEntities.assertCustomersFields;
import static util.AssertionsCaseForEntities.assertCustomersListFields;

import entity.CustomerEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import repository.CustomerRepository;
import repository.OrderRepository;
import service.impl.CustomerServiceImpl;
import service.impl.OrderServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CustomerServiceTest {

  @Mock
  private CustomerRepository customerRepository;

  @Mock
  private OrderRepository orderRepository;

  private CustomerServiceImpl customerService;
  private OrderService orderService;

  @BeforeEach
  public void setUp() {
    orderService = new OrderServiceImpl(orderRepository);
    customerService = new CustomerServiceImpl(customerRepository, orderService);
  }

  @Test
  public void getById_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();
    when(customerRepository.findById(generatedCustomer.getId()))
        .thenReturn(Optional.of(generatedCustomer));

    //when
    final CustomerEntity foundCustomer = customerService.getById(generatedCustomer.getId());

    //when
    assertCustomersFields(generatedCustomer, foundCustomer);
    Assertions.assertEquals(generatedCustomer.getId(), foundCustomer.getId());
  }

  @Test
  public void getAll_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();
    when(customerRepository.findAll()).thenReturn(List.of(generatedCustomer));

    //when
    final List<CustomerEntity> foundCustomers = customerService.getAll();

    //then
    assertCustomersListFields(generatedCustomer, foundCustomers);
  }

  @Test
  public void create_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();
    when(customerRepository.create(any())).thenReturn(generatedCustomer.getId());
    when(customerRepository.findById(generatedCustomer.getId()))
        .thenReturn(Optional.of(generatedCustomer));

    //when
    final CustomerEntity foundCustomer = customerService.create(generatedCustomer);

    //then
    assertCustomersFields(generatedCustomer, foundCustomer);
  }

  @Test
  public void updateById_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();
    when(customerRepository.findById(generatedCustomer.getId()))
        .thenReturn(Optional.of(generatedCustomer));
    generatedCustomer.setFirstName("Ron");
    when(customerRepository.update(generatedCustomer)).thenReturn(Optional.of(generatedCustomer));

    //when
    final CustomerEntity updatedCustomer = customerService.updateById(generatedCustomer.getId(),
        mapToCustomerModel(generatedCustomer));

    //then
    assertCustomersFields(generatedCustomer, updatedCustomer);
    Assertions.assertEquals("Ron", generatedCustomer.getFirstName());
  }

  @Test
  public void deleteById_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();
    when(customerRepository.findById(generatedCustomer.getId()))
        .thenReturn(Optional.of(generatedCustomer));
    doNothing().when(customerRepository).delete(generatedCustomer);

    //when
    customerService.deleteById(generatedCustomer.getId());

    //then
    verify(customerRepository, times(1));
    Assertions.assertThrows(NullPointerException.class,
        () -> customerService.getById(generatedCustomer.getId()));
  }

  @Test
  public void getById_whenNotFound() {
    //given
    final UUID notExistingId = UUID.randomUUID();
    when(customerRepository.findById(notExistingId))
        .thenThrow(new EntityNotFoundException(CustomerEntity.class.getName(), notExistingId));

    //when
    final Exception exception = Assertions
        .assertThrows(EntityNotFoundException.class, () -> customerService.getById(notExistingId));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
  }

  @Test
  public void updateById_whenNotFound() {
    //given
    final UUID notExistingId = UUID.randomUUID();
    final CustomerEntity generatedCustomer = generateCustomer();
    when(customerRepository.findById(notExistingId))
        .thenThrow(new EntityNotFoundException(CustomerEntity.class.getName(), notExistingId));
    when(customerRepository.update(generatedCustomer))
        .thenThrow(new EntityNotUpdatedException(CustomerEntity.class.getName(), notExistingId));

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> customerService.updateById(notExistingId, mapToCustomerModel(generatedCustomer)));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
  }

  @Test
  public void deleteById_whenNotFound() {
    //given
    final UUID notExistingId = UUID.randomUUID();
    final CustomerEntity generatedCustomer = generateCustomer();
    generatedCustomer.setId(notExistingId);
    when(customerRepository.findById(notExistingId))
        .thenThrow(new EntityNotFoundException(CustomerEntity.class.getName(), notExistingId));
    doNothing().when(customerRepository).delete(generatedCustomer);

    //when
    final Exception exception = Assertions
        .assertThrows(EntityNotFoundException.class,
            () -> customerService.deleteById(notExistingId));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
  }
}
