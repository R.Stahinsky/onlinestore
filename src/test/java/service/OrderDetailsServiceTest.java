package service;

import static mapper.OrderDetailsMapper.mapToOrderDetailsModel;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static service.util.EntityCreatorUtil.createOrderDetailsWithEntities;
import static util.AssertionsCaseForEntities.assertOrderDetailsFields;
import static util.AssertionsCaseForEntities.assertOrderDetailsListFields;

import entity.OrderDetailsEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import repository.OrderDetailsRepository;
import service.impl.OrderDetailsServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class OrderDetailsServiceTest {

  @Mock
  private OrderDetailsRepository orderDetailsRepository;

  private OrderDetailsService orderDetailsService;

  @BeforeEach
  public void setUp() {
    orderDetailsService = new OrderDetailsServiceImpl(orderDetailsRepository);
  }

  @Test
  public void getById_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(orderDetailsRepository.findById(generatedOrderDetails.getId()))
        .thenReturn(Optional.of(generatedOrderDetails));

    //when
    final OrderDetailsEntity foundOrderDetails =
        orderDetailsService.getById(generatedOrderDetails.getId());

    //then
    assertOrderDetailsFields(generatedOrderDetails, foundOrderDetails);
    Assertions.assertEquals(generatedOrderDetails.getId(), foundOrderDetails.getId());
  }

  @Test
  public void getAll_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(orderDetailsRepository.findAll()).thenReturn(List.of(generatedOrderDetails));

    //when
    final List<OrderDetailsEntity> foundOrderDetails = orderDetailsService.getAll();

    //then
    assertOrderDetailsListFields(generatedOrderDetails, foundOrderDetails);
  }

  @Test
  public void create_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(orderDetailsRepository.findById(generatedOrderDetails.getId()))
        .thenReturn(Optional.of(generatedOrderDetails));
    when(orderDetailsRepository.create(generatedOrderDetails))
        .thenReturn(generatedOrderDetails.getId());

    //when
    final OrderDetailsEntity savedOrderDetails = orderDetailsService.create(generatedOrderDetails);

    //then
    assertOrderDetailsFields(generatedOrderDetails, savedOrderDetails);
  }

  @Test
  public void updateById_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(orderDetailsRepository.findById(generatedOrderDetails.getId()))
        .thenReturn(Optional.of(generatedOrderDetails));
    generatedOrderDetails.setCount(8);
    when(orderDetailsRepository.update(generatedOrderDetails))
        .thenReturn(Optional.of(generatedOrderDetails));

//    when
    final OrderDetailsEntity updatedOrderDetails = orderDetailsService
        .updateById(generatedOrderDetails.getId(), mapToOrderDetailsModel(generatedOrderDetails));

    //then
    assertOrderDetailsFields(generatedOrderDetails, updatedOrderDetails);
    Assertions.assertEquals(generatedOrderDetails.getId(), updatedOrderDetails.getId());
  }

  @Test
  public void deleteById_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(orderDetailsRepository.findById(generatedOrderDetails.getId()))
        .thenReturn(Optional.of(generatedOrderDetails));
    doNothing().when(orderDetailsRepository).delete(generatedOrderDetails);

    //when
    orderDetailsService.deleteById(generatedOrderDetails.getId());

    //then
    verify(orderDetailsRepository, times(1));
    Assertions.assertThrows(NullPointerException.class,
        () -> orderDetailsService.getById(generatedOrderDetails.getId()));
  }

  @Test
  public void getAllByOrderId_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(orderDetailsRepository.findAllByOrderId(generatedOrderDetails.getOrder().getId()))
        .thenReturn(List.of(generatedOrderDetails));

    //when
    final List<OrderDetailsEntity> foundOrderDetails =
        orderDetailsService.getAllByOrderId(generatedOrderDetails.getOrder().getId());

    //then
    assertOrderDetailsListFields(generatedOrderDetails, foundOrderDetails);
  }

  @Test
  public void getAllByProductId_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(orderDetailsRepository.findAllByProductId(generatedOrderDetails.getProduct().getId()))
        .thenReturn(List.of(generatedOrderDetails));

    //when
    final List<OrderDetailsEntity> foundOrderDetails =
        orderDetailsService.getAllByProductId(generatedOrderDetails.getProduct().getId());

    //then
    assertOrderDetailsListFields(generatedOrderDetails, foundOrderDetails);
  }

  @Test
  public void getById_whenNotFound() {
    //given
    final UUID notExistingId = UUID.randomUUID();
    when(orderDetailsRepository.findById(notExistingId))
        .thenThrow(new EntityNotFoundException(OrderDetailsEntity.class.getName(), notExistingId));

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> orderDetailsService.getById(notExistingId));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
  }

  @Test
  public void updateById_whenNotFound() {
    //given
    final UUID notExisting = UUID.randomUUID();
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(orderDetailsRepository.findById(notExisting))
        .thenThrow(new EntityNotFoundException(OrderDetailsEntity.class.getName(), notExisting));
    when(orderDetailsRepository.update(generatedOrderDetails))
        .thenThrow(new EntityNotUpdatedException(OrderDetailsEntity.class.getName(), notExisting));

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> orderDetailsService
            .updateById(notExisting, mapToOrderDetailsModel(generatedOrderDetails)));
    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
  }

  @Test
  public void deleteById_whenNotFound() {
    //given
    final UUID notExisting = UUID.randomUUID();
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    when(orderDetailsRepository.findById(notExisting))
        .thenThrow(new EntityNotFoundException(OrderDetailsEntity.class.getName(), notExisting));
    doNothing().when(orderDetailsRepository).delete(generatedOrderDetails);

    //when
    final Exception exception = Assertions.assertThrows(EntityNotFoundException.class,
        () -> orderDetailsService.deleteById(notExisting));

    //then
    Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
  }
}
