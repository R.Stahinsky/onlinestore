package service.util;

import com.github.javafaker.Faker;
import entity.CustomerEntity;
import entity.OrderDetailsEntity;
import entity.OrderEntity;
import entity.ProductCategoryEntity;
import entity.ProductEntity;
import enums.Gender;
import enums.OrderStatus;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class EntityGeneratorUtil {

  private static final Faker FAKER = new Faker();

  public static CustomerEntity generateCustomer() {
    return CustomerEntity.builder()
        .id(UUID.randomUUID())
        .firstName(FAKER.name().firstName())
        .lastName(FAKER.name().lastName())
        .email(FAKER.internet().emailAddress())
        .address(FAKER.address().fullAddress())
        .gender(Gender.MALE)
        .birthDate((FAKER.date().birthday()).toInstant())
        .createdAt(Instant.now().truncatedTo(ChronoUnit.SECONDS))
        .updatedAt(Instant.now().truncatedTo(ChronoUnit.SECONDS))
        .build();
  }

  public static OrderDetailsEntity generateOrderDetails() {
    return OrderDetailsEntity.builder()
        .id(UUID.randomUUID())
        .product(generateProduct())
        .count(FAKER.number().randomDigit())
        .build();
  }

  public static OrderEntity generateOrder() {
    return OrderEntity.builder()
        .id(UUID.randomUUID())
        .customer(generateCustomer())
        .orderDetails(List.of(generateOrderDetails()))
        .orderNumber(FAKER.number().randomDigit())
        .orderDate(FAKER.date().past(200, TimeUnit.DAYS, Date.from(Instant.now())).toInstant())
        .price(FAKER.number().randomDouble(2, 0, 999))
        .address(FAKER.address().fullAddress())
        .status(OrderStatus.COMPLETED)
        .createdAt(Instant.now().truncatedTo(ChronoUnit.SECONDS))
        .updatedAt(Instant.now().truncatedTo(ChronoUnit.SECONDS))
        .build();
  }

  public static ProductCategoryEntity generateProductCategory() {
    return ProductCategoryEntity.builder()
        .id(UUID.randomUUID())
        .name(FAKER.food().sushi())
        .build();
  }

  public static ProductEntity generateProduct() {
    return ProductEntity.builder()
        .id(UUID.randomUUID())
        .productCategory(generateProductCategory())
        .name(FAKER.food().fruit())
        .price(FAKER.number().randomDouble(2, 0, 999))
        .description(FAKER.color().name())
        .createdAt(Instant.now().truncatedTo(ChronoUnit.SECONDS))
        .updatedAt(Instant.now().truncatedTo(ChronoUnit.SECONDS))
        .build();
  }
}
