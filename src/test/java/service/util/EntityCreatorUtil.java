package service.util;

import static service.util.EntityGeneratorUtil.generateCustomer;
import static service.util.EntityGeneratorUtil.generateOrder;
import static service.util.EntityGeneratorUtil.generateOrderDetails;
import static service.util.EntityGeneratorUtil.generateProduct;
import static service.util.EntityGeneratorUtil.generateProductCategory;
import entity.CustomerEntity;
import entity.OrderDetailsEntity;
import entity.OrderEntity;
import entity.ProductCategoryEntity;
import entity.ProductEntity;
import java.util.List;
import java.util.UUID;

public class EntityCreatorUtil {

  public static OrderDetailsEntity createOrderDetailsWithEntities() {
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();
    final CustomerEntity generatedCustomer = generateCustomer();
    final ProductEntity generatedProduct = generateProduct();
    final OrderEntity generatedOrder = generateOrder();
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();

    generatedOrder.setCustomer(generatedCustomer);
    generatedProduct.setProductCategory(generatedProductCategory);
    generatedOrderDetails.setOrder(generatedOrder);
    generatedOrderDetails.setProduct(generatedProduct);

    return generatedOrderDetails;
  }

  public static OrderEntity createOrderWithEntities() {
    final OrderEntity generatedOrder = generateOrder();
    final OrderDetailsEntity generatedOrderDetails = createOrderDetailsWithEntities();
    final ProductEntity generatedProduct = generateProduct();

    generatedOrderDetails.setProduct(generatedProduct);
    generatedOrder.setOrderDetails(List.of(generatedOrderDetails));

    return generatedOrder;
  }

  public static ProductEntity createProductWithEntities(){
    final ProductEntity generatedProduct = generateProduct();
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();

    generatedProductCategory.setId(UUID.randomUUID());
    generatedProduct.setId(UUID.randomUUID());
    generatedProduct.setProductCategory(generatedProductCategory);

    return generatedProduct;
  }
}
