package repository;

import static repository.util.EntityGeneratorUtil.generateCustomer;
import static repository.util.EntityGeneratorUtil.generateOrder;
import static repository.util.EntityGeneratorUtil.generateProduct;
import static repository.util.EntityGeneratorUtil.generateProductCategory;

import entity.CustomerEntity;
import entity.OrderDetailsEntity;
import entity.OrderEntity;
import entity.ProductCategoryEntity;
import entity.ProductEntity;
import exceptions.EntityNotFoundException;
import java.util.List;
import java.util.UUID;
import repository.impl.CustomerRepositoryImpl;
import repository.impl.OrderDetailsRepositoryImpl;
import repository.impl.OrderRepositoryImpl;
import repository.impl.ProductCategoryRepositoryImpl;
import repository.impl.ProductRepositoryImpl;

public abstract class AbstractRepositoryTest {

  protected static final CustomerRepository customerRepository = new CustomerRepositoryImpl();
  protected static final OrderDetailsRepository orderDetailsRepository = new OrderDetailsRepositoryImpl();
  protected static final OrderRepository orderRepository = new OrderRepositoryImpl();
  protected static final ProductCategoryRepository productCategoryRepository = new ProductCategoryRepositoryImpl();
  protected static final ProductRepository productRepository = new ProductRepositoryImpl();

  protected static CustomerEntity createTestCustomer(final CustomerEntity generatedCustomer) {
    final UUID orderId = orderRepository.create(generateOrder());
    final OrderEntity savedOrder = orderRepository.findById(orderId)
        .orElseThrow(() -> new EntityNotFoundException(OrderEntity.class.getName(), orderId));

    final List<OrderEntity> savedOrders = List.of(savedOrder);
    generatedCustomer.setOrders(savedOrders);
    final UUID customerId = customerRepository.create(generatedCustomer);

    return customerRepository.findById(customerId)
        .orElseThrow(() -> new EntityNotFoundException(CustomerEntity.class.getName(), customerId));
  }

  protected static OrderDetailsEntity createOrderDetailsWithoutSaving(
      final OrderDetailsEntity generatedOrderDetails) {
    final ProductEntity productToSave = generateProduct();
    final OrderEntity orderToSave = generateOrder();

    final UUID savedCustomerId = customerRepository.create(generateCustomer());
    final UUID savedProductCategoryId = productCategoryRepository.create(generateProductCategory());

    final CustomerEntity foundCustomer = customerRepository.findById(savedCustomerId)
        .orElseThrow(
            () -> new EntityNotFoundException(CustomerEntity.class.getName(), savedCustomerId));

    orderToSave.setCustomer(foundCustomer);

    final UUID savedOrderId = orderRepository.create(orderToSave);
    final OrderEntity foundOrder = orderRepository.findById(savedOrderId)
        .orElseThrow(
            () -> new EntityNotFoundException(OrderEntity.class.getName(), savedOrderId));

    generatedOrderDetails.setOrder(foundOrder);

    final ProductCategoryEntity foundProductCategory = productCategoryRepository
        .findById(savedProductCategoryId)
        .orElseThrow(() -> new EntityNotFoundException(ProductCategoryEntity.class.getName(),
            savedProductCategoryId));
    productToSave.setProductCategory(foundProductCategory);

    final UUID savedProductId = productRepository.create(productToSave);
    final ProductEntity foundProduct = productRepository.findById(savedProductId).orElseThrow();

    generatedOrderDetails.setProduct(foundProduct);

    return generatedOrderDetails;
  }

  protected static OrderDetailsEntity createTestOrderDetailsWithEntities(
      final OrderDetailsEntity generatedOrderDetails) {

    final UUID savedOrderDetailsId =
        orderDetailsRepository.create(createOrderDetailsWithoutSaving(generatedOrderDetails));

    return orderDetailsRepository
        .findById(savedOrderDetailsId)
        .orElseThrow(() -> new EntityNotFoundException(OrderDetailsEntity.class.getName(),
            savedOrderDetailsId));
  }

  protected static OrderEntity createTestOrderWithCustomer(final OrderEntity generatedOrder) {
    final UUID orderId = orderRepository.create(createOrderWithoutSaving(generatedOrder));

    return orderRepository.findById(orderId)
        .orElseThrow(() -> new EntityNotFoundException(OrderEntity.class.getName(), orderId));
  }

  protected static OrderEntity createOrderWithoutSaving(final OrderEntity generatedOrder) {
    final UUID customerId = customerRepository.create(generateCustomer());
    final CustomerEntity foundCustomer = customerRepository.findById(customerId)
        .orElseThrow(() -> new EntityNotFoundException(CustomerEntity.class.getName(), customerId));

    generatedOrder.setCustomer(foundCustomer);

    return generatedOrder;
  }

  protected static ProductCategoryEntity createTestProductCategory(
      final ProductCategoryEntity generatedProductCategory) {

    final UUID savedProductCategoryId = productCategoryRepository.create(generatedProductCategory);

    return productCategoryRepository.findById(savedProductCategoryId)
        .orElseThrow(() -> new EntityNotFoundException(ProductCategoryEntity.class.getName(),
            savedProductCategoryId));
  }

  protected static ProductEntity createTestProductWithProductCategory(
      final ProductEntity generatedProduct) {
    final UUID savedProductId = productRepository
        .create(createProductWithoutSaving(generatedProduct));

    return productRepository.findById(savedProductId).orElseThrow(
        () -> new EntityNotFoundException(ProductEntity.class.getName(), savedProductId));
  }

  protected static ProductEntity createProductWithoutSaving(final ProductEntity generatedProduct) {
    final UUID savedProductCategoryId = productCategoryRepository.create(generateProductCategory());
    final ProductCategoryEntity foundProductCategory = productCategoryRepository
        .findById(savedProductCategoryId).orElseThrow(
            () -> new EntityNotFoundException(ProductCategoryEntity.class.getName(),
                savedProductCategoryId));

    generatedProduct.setProductCategory(foundProductCategory);

    return generatedProduct;
  }
}
