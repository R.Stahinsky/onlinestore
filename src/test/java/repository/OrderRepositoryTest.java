package repository;

import static util.AssertionsCaseForEntities.assertOrderListFields;
import static util.AssertionsCaseForEntities.assertOrdersFields;
import static repository.util.EntityGeneratorUtil.generateOrder;
import entity.OrderEntity;
import exceptions.EntityNotUpdatedException;
import java.time.Instant;
import java.util.List;
import javax.persistence.NoResultException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OrderRepositoryTest extends AbstractRepositoryTest{

  @BeforeEach
  public void setUp() {
    orderDetailsRepository.deleteAll();
    orderRepository.deleteAll();
    customerRepository.deleteAll();
  }

  @Test
  public void findById_happyPath() {
    //given
    final OrderEntity generatedOrder = generateOrder();

    //when
    final OrderEntity savedOrder = createTestOrderWithCustomer(generatedOrder);

    //then
    assertOrdersFields(generatedOrder, savedOrder);
  }

  @Test
  public void findAll_happyPath() {
    //given
    final OrderEntity generatedOrder = generateOrder();
    final OrderEntity savedOrder = createTestOrderWithCustomer(generatedOrder);

    //when
    final List<OrderEntity> foundOrders = orderRepository.findAll();

    //then
    assertOrderListFields(savedOrder, foundOrders);
  }

  @Test
  public void create_happyPath() {
    //given
    final OrderEntity generatedOrder = generateOrder();

    //when
    final OrderEntity savedOrder = createTestOrderWithCustomer(generatedOrder);

    //then
    assertOrdersFields(generatedOrder, savedOrder);
  }

  @Test
  public void update_happyPath() {
    //given
    final OrderEntity generatedOrder = generateOrder();
    final OrderEntity orderForUpdate = createTestOrderWithCustomer(generatedOrder);
    orderForUpdate.setOrderNumber(17);

    //when
    final OrderEntity updatedOrder = orderRepository.update(orderForUpdate)
        .orElseThrow(() -> new EntityNotUpdatedException(OrderEntity.class.getName(),
            orderForUpdate.getId()));

    //then
    assertOrdersFields(orderForUpdate, updatedOrder);
    Assertions.assertEquals(17, updatedOrder.getOrderNumber());
    Assertions.assertEquals(orderForUpdate.getId(), updatedOrder.getId());
  }

  @Test
  public void delete_happyPath() {
    //given
    final OrderEntity generatedOrder = generateOrder();
    final OrderEntity orderForDelete = createTestOrderWithCustomer(generatedOrder);
    orderForDelete.setDeletedAt(Instant.now());

    //when
    orderRepository.delete(orderForDelete);

    //then
    Assertions.assertThrows(NoResultException.class,
        () -> orderRepository.findById(orderForDelete.getId()));
  }

  @Test
  public void findAllBeforeDate_happyPath() {
    //given
    final OrderEntity generatedOrder = generateOrder();
    final OrderEntity savedOrder = createTestOrderWithCustomer(generatedOrder);
    final Instant date = Instant.parse("2018-10-08T00:00:00.00000Z");

    //when
    final List<OrderEntity> foundOrders = orderRepository
        .findAllBeforeDate(date);

    //then
    assertOrderListFields(savedOrder, foundOrders);
  }

  @Test
  public void countAveragePriceBeforeDate_happyPath() {
    //given
    final OrderEntity generatedOrder = generateOrder();
    createTestOrderWithCustomer(generatedOrder);
    final Instant date = Instant.parse("2018-10-08T00:00:00.00000Z");

    //when
    final double averagePrice = orderRepository
        .countAveragePriceBeforeDate(date);

    //then
    Assertions.assertNotNull(averagePrice);
    Assertions.assertEquals(generatedOrder.getPrice(), averagePrice);
  }

  @Test
  public void findAllByCustomerId_happyPath() {
    //given
    final OrderEntity generatedOrder = generateOrder();
    final OrderEntity savedOrder = createTestOrderWithCustomer(generatedOrder);

    //when
    final List<OrderEntity> foundOrders = orderRepository
        .findAllByCustomerId(savedOrder.getCustomer().getId());

    //then
    assertOrderListFields(savedOrder, foundOrders);
  }
}
