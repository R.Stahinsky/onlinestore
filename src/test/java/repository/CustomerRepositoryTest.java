package repository;

import static util.AssertionsCaseForEntities.assertCustomersFields;
import static util.AssertionsCaseForEntities.assertCustomersListFields;
import static repository.util.EntityGeneratorUtil.generateCustomer;

import entity.CustomerEntity;
import exceptions.EntityNotUpdatedException;
import java.time.Instant;
import java.util.List;
import javax.persistence.NoResultException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CustomerRepositoryTest extends AbstractRepositoryTest{

  @BeforeEach
  public void setUp() {
    orderDetailsRepository.deleteAll();
    orderRepository.deleteAll();
    customerRepository.deleteAll();
  }

  @Test
  public void findById_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();

    //when
    final CustomerEntity savedCustomer = createTestCustomer(generatedCustomer);

    //then
    assertCustomersFields(generatedCustomer, savedCustomer);
  }

  @Test
  public void findAll_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();
    final CustomerEntity savedCustomer = createTestCustomer(generatedCustomer);

    //when
    final List<CustomerEntity> foundCustomers = customerRepository.findAll();

    //then
    assertCustomersListFields(savedCustomer, foundCustomers);
  }

  @Test
  public void create_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();

    //when
    final CustomerEntity savedCustomer = createTestCustomer(generatedCustomer);

    //then
    assertCustomersFields(generatedCustomer, savedCustomer);
  }

  @Test
  public void update_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();
    final CustomerEntity customerForUpdate = createTestCustomer(generatedCustomer);
    customerForUpdate.setFirstName("Ron");

    //when
    final CustomerEntity updatedCustomer = customerRepository
        .update(customerForUpdate)
        .orElseThrow(() -> new EntityNotUpdatedException(CustomerEntity.class.getName(),
            customerForUpdate.getId()));

    //then
    assertCustomersFields(customerForUpdate, updatedCustomer);
    Assertions.assertEquals("Ron", customerForUpdate.getFirstName());
    Assertions.assertEquals(customerForUpdate.getId(), updatedCustomer.getId());
  }

  @Test
  public void deleteById_happyPath() {
    //given
    final CustomerEntity generatedCustomer = generateCustomer();
    final CustomerEntity customerForDelete = createTestCustomer(generatedCustomer);
    customerForDelete.setDeletedAt(Instant.now());

    //when
    customerRepository.delete(customerForDelete);

    //then
    Assertions.assertThrows(NoResultException.class,
        () -> customerRepository.findById(customerForDelete.getId()));
  }
}
