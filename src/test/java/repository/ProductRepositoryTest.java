package repository;

import static util.AssertionsCaseForEntities.assertProductsFields;
import static util.AssertionsCaseForEntities.assertProductsListFields;
import static repository.util.EntityGeneratorUtil.generateProduct;
import entity.ProductEntity;
import exceptions.EntityNotUpdatedException;
import java.time.Instant;
import java.util.List;
import javax.persistence.NoResultException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductRepositoryTest extends AbstractRepositoryTest{

  @BeforeEach
  public void setUp() {
    productRepository.deleteAll();
    productCategoryRepository.deleteAll();
  }

  @Test
  public void findById_happyPath() {
    //given
    final ProductEntity generatedProduct = generateProduct();

    //when
    final ProductEntity savedProduct = createTestProductWithProductCategory(generatedProduct);

    //then
    assertProductsFields(generatedProduct, savedProduct);
  }

  @Test
  public void findAll_happyPath() {
    //given
    final ProductEntity generatedProduct = generateProduct();
    final ProductEntity savedProduct = createTestProductWithProductCategory(generatedProduct);

    //when
    final List<ProductEntity> foundProducts = productRepository.findAll();

    //then
    assertProductsListFields(savedProduct, foundProducts);
  }

  @Test
  public void create_happyPath() {
    //given
    final ProductEntity generatedProduct = generateProduct();

    //when
    final ProductEntity savedProduct = createTestProductWithProductCategory(generatedProduct);

    //then
    assertProductsFields(generatedProduct, savedProduct);
  }

  @Test
  public void update_happyPath() {
    //given
    final ProductEntity generatedProduct = generateProduct();
    final ProductEntity productForUpdate = createTestProductWithProductCategory(generatedProduct);
    productForUpdate.setPrice(4.5);

    //when
    final ProductEntity updatedProduct = productRepository.update(productForUpdate)
        .orElseThrow(() -> new EntityNotUpdatedException(ProductEntity.class.getName(),
            productForUpdate.getId()));

    //then
    assertProductsFields(productForUpdate, updatedProduct);
    Assertions.assertEquals(4.5, updatedProduct.getPrice());
    Assertions.assertEquals(productForUpdate.getId(), updatedProduct.getId());
  }

  @Test
  public void delete_happyPath() {
    //given
    final ProductEntity generatedProduct = generateProduct();
    final ProductEntity productForDelete = createTestProductWithProductCategory(generatedProduct);
    productForDelete.setDeletedAt(Instant.now());

    //when
    productRepository.delete(productForDelete);

    //then
    Assertions.assertThrows(NoResultException.class,
        () -> productRepository.findById(productForDelete.getId()));
  }

  @Test
  public void findAllBeforeDate_happyPath() {
    //given
    final ProductEntity generatedProduct = generateProduct();
    final ProductEntity savedProduct = createTestProductWithProductCategory(generatedProduct);
    final Instant date = Instant.parse("2021-10-08T00:00:00.00000Z");

    //when
    final List<ProductEntity> foundProducts = productRepository
        .findAllBeforeDate(date);

    //then
    assertProductsListFields(savedProduct, foundProducts);
  }
}
