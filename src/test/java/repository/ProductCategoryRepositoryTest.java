package repository;

import static util.AssertionsCaseForEntities.assertProductCategoriesFields;
import static repository.util.EntityGeneratorUtil.generateProductCategory;
import entity.ProductCategoryEntity;
import exceptions.EntityNotUpdatedException;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductCategoryRepositoryTest extends AbstractRepositoryTest{

  @BeforeEach
  public void setUp() {
    productRepository.deleteAll();
    productCategoryRepository.deleteAll();
  }

  @Test
  public void findById_happyPath() {
    //given
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();

    //when
    final ProductCategoryEntity savedProductCategory =
        createTestProductCategory(generatedProductCategory);

    //then
    assertProductCategoriesFields(generatedProductCategory, savedProductCategory);
  }

  @Test
  public void findAll_happyPath() {
    //given
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();
    final ProductCategoryEntity savedProductCategory =
        createTestProductCategory(generatedProductCategory);

    //when
    final List<ProductCategoryEntity> foundProductCategories = productCategoryRepository.findAll();

    //then
    Assertions.assertNotNull(foundProductCategories);
    Assertions.assertEquals(1, foundProductCategories.size());
    Assertions.
        assertEquals(savedProductCategory.getId(),
            foundProductCategories.stream().findFirst().get().getId());
    Assertions.
        assertEquals(savedProductCategory.getName(),
            foundProductCategories.stream().findFirst().get().getName());
  }

  @Test
  public void create_happyPath() {
    //given
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();

    //when
    final ProductCategoryEntity savedProductCategory =
        createTestProductCategory(generatedProductCategory);

    //then
    assertProductCategoriesFields(generatedProductCategory, savedProductCategory );
  }

  @Test
  public void update_happyPath() {
    //given
    final ProductCategoryEntity generateProductCategory = generateProductCategory();
    final ProductCategoryEntity productCategoryForUpdate =
        createTestProductCategory(generateProductCategory);
    productCategoryForUpdate.setName("Beer");

    //when
    final ProductCategoryEntity updatedProductCategory = productCategoryRepository
        .update(productCategoryForUpdate).orElseThrow(
            () -> new EntityNotUpdatedException(ProductCategoryEntity.class.getName(),
                productCategoryForUpdate.getId()));

    //then
    Assertions.assertEquals("Beer", updatedProductCategory.getName());
    assertProductCategoriesFields(productCategoryForUpdate, updatedProductCategory);
    Assertions.assertEquals(updatedProductCategory.getId(), productCategoryForUpdate.getId());
  }

  @Test
  public void delete_happyPath() {
    //given
    final ProductCategoryEntity generatedProductCategory = generateProductCategory();
    final ProductCategoryEntity savedProductCategory =
        createTestProductCategory(generatedProductCategory);

    //when
    productCategoryRepository.delete(savedProductCategory);

    //then
    Assertions.assertThrows(NullPointerException.class,
        () -> productCategoryRepository.findById(savedProductCategory.getId()));
  }
}
