package repository;

import static util.AssertionsCaseForEntities.assertOrderDetailsFields;
import static util.AssertionsCaseForEntities.assertOrderDetailsListFields;
import static repository.util.EntityGeneratorUtil.generateOrderDetails;
import entity.OrderDetailsEntity;
import exceptions.EntityNotUpdatedException;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OrderDetailsRepositoryTest extends AbstractRepositoryTest {

  @BeforeEach
  public void setUp() {
    orderDetailsRepository.deleteAll();
    productRepository.deleteAll();
    productCategoryRepository.deleteAll();
    orderRepository.deleteAll();
    customerRepository.deleteAll();
  }

  @Test
  public void findById_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();

    //when
    final OrderDetailsEntity savedOrderDetails =
        createTestOrderDetailsWithEntities(generatedOrderDetails);

    //then
    assertOrderDetailsFields(generatedOrderDetails, savedOrderDetails);
  }

  @Test
  public void findAll_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();
    final OrderDetailsEntity savedOrderDetails = createTestOrderDetailsWithEntities(
        generatedOrderDetails);

    //when
    final List<OrderDetailsEntity> foundOrderDetails = orderDetailsRepository.findAll();

    //then
    assertOrderDetailsListFields(savedOrderDetails, foundOrderDetails);
  }

  @Test
  public void create_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();

    //when
    final OrderDetailsEntity savedOrderDetails = createTestOrderDetailsWithEntities(
        generatedOrderDetails);

    //then
    assertOrderDetailsFields(generatedOrderDetails, savedOrderDetails);
  }

  @Test
  public void update_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();
    final OrderDetailsEntity orderDetailsForUpdate
        = createTestOrderDetailsWithEntities(generatedOrderDetails);

    orderDetailsForUpdate.setCount(8);

    //when
    final OrderDetailsEntity updatedOrderDetails = orderDetailsRepository
        .update(orderDetailsForUpdate).orElseThrow(
            () -> new EntityNotUpdatedException(OrderDetailsEntity.class.getName(),
                orderDetailsForUpdate.getId()));

    //then
    Assertions.assertEquals(8, updatedOrderDetails.getCount());
    assertOrderDetailsFields(orderDetailsForUpdate, updatedOrderDetails);
    Assertions.assertEquals(orderDetailsForUpdate.getId(), updatedOrderDetails.getId());
  }

  @Test
  public void delete_happyPath() {
    //given
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();
    final OrderDetailsEntity savedOrderDetails =
        createTestOrderDetailsWithEntities(generatedOrderDetails);

    //when
    orderDetailsRepository.delete(savedOrderDetails);

    //then
    Assertions.assertThrows(NullPointerException.class,
        () -> orderDetailsRepository.findById(savedOrderDetails.getId()));
  }

  @Test
  public void findAllByOrderId() {
    //given
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();
    final OrderDetailsEntity savedOrderDetails =
        createTestOrderDetailsWithEntities(generatedOrderDetails);

    //when
    final List<OrderDetailsEntity> foundOrderDetails = orderDetailsRepository
        .findAllByOrderId(savedOrderDetails.getOrder().getId());

    //then
    assertOrderDetailsListFields(savedOrderDetails, foundOrderDetails);
  }

  @Test
  public void findAllByProductId() {
    //given
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();
    final OrderDetailsEntity savedOrderDetails =
        createTestOrderDetailsWithEntities(generatedOrderDetails);

    //when
    final List<OrderDetailsEntity> foundOrderDetails = orderDetailsRepository
        .findAllByProductId((savedOrderDetails.getProduct().getId()));

    //then
    assertOrderDetailsListFields(savedOrderDetails, foundOrderDetails);
  }

  @Test
  public void countByProductId() {
    //given
    final OrderDetailsEntity generatedOrderDetails = generateOrderDetails();
    final OrderDetailsEntity savedOrderDetails =
        createTestOrderDetailsWithEntities(generatedOrderDetails);

    //when
    final int count = orderDetailsRepository
        .countByProductId(savedOrderDetails.getProduct().getId());

    //then
    Assertions.assertNotNull(count);
    Assertions.assertEquals(generatedOrderDetails.getCount(), count);
    assertOrderDetailsFields(generatedOrderDetails, savedOrderDetails);
  }
}
