CREATE TYPE gender_enum AS ENUM ('MALE', 'FEMALE');
CREATE TABLE IF NOT EXISTS public.customer
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    first_name text NOT NULL,
    last_name text NOT NULL,
    email text NOT NULL UNIQUE,
    address text NOT NULL UNIQUE,
    gender gender_enum NOT NULL,
    birth_date timestamp NOT NULL,
    created_at timestamp DEFAULT NOW() NOT NULL,
    updated_at timestamp NOT NULL,
    deleted_at timestamp
);
ALTER TABLE public.customer
    OWNER to postgres;


CREATE TABLE IF NOT EXISTS public.product_category
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    name text NOT NULL UNIQUE
);
ALTER TABLE public.product_category
    OWNER to postgres;


CREATE TABLE IF NOT EXISTS public.product
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    name text NOT NULL UNIQUE,
    price double precision NOT NULL,
    description text,
    product_category_id uuid CONSTRAINT product_category_id_fkey REFERENCES product_category (id) NOT NULL,
    created_at timestamp DEFAULT NOW() NOT NULL,
    updated_at timestamp NOT NULL,
    deleted_at timestamp
);
ALTER TABLE public.product
    OWNER to postgres;


CREATE TYPE order_status AS enum ('COMPLETED', 'PROCESSING', 'ON_HOLD');
CREATE TABLE IF NOT EXISTS public.customer_order
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    order_number bigint NOT NULL UNIQUE,
    customer_id uuid CONSTRAINT customer_id_fkey REFERENCES customer (id),
    date timestamp NOT NULL,
    price double precision NOT NULL,
    address text NOT NULL,
    status order_status NOT NULL,
    created_at timestamp DEFAULT NOW() NOT NULL,
    updated_at timestamp NOT NULL,
    deleted_at timestamp
);
ALTER TABLE public.customer_order
    OWNER to postgres;


CREATE TABLE IF NOT EXISTS public.order_details
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    order_id uuid CONSTRAINT order_id_fkey REFERENCES customer_order (id) NOT NULL,
    product_id uuid CONSTRAINT product_id_fkey REFERENCES product (id) NOT NULL,
    count bigint NOT NULL
);

ALTER TABLE public.order_details
    OWNER to postgres;
