INSERT INTO customer (first_name, last_name, email, address, gender,
                                                    birth_date, created_at, updated_at, deleted_at)
VALUES ('Dan', 'Bobson', 'resort@gmail.com', 'Minskaya str1.', 'MALE','1987-01-08',
                                                    '2000-11-08', '2000-11-08', NULL);

INSERT INTO customer_order (order_number, customer_id, date, price,
                                               address, status, created_at, updated_at, deleted_at)
VALUES (2, (SELECT id FROM customer WHERE email = 'resort@gmail.com'),
        '2021-08-24', 12.4, 'Minskaya str.', 'COMPLETED', '2000-11-08', '2000-11-08', '2000-11-08');

INSERT INTO product (name, price, description, product_category_id, created_at, updated_at, deleted_at)
VALUES ('Apple', 2.4, 'Sweet', (SELECT id FROM product_category WHERE name = 'Fruits'),
                                                          '2000-11-08', '2000-11-08', NULL);

INSERT INTO order_details (order_id, product_id, count)
VALUES ((SELECT id FROM customer_order WHERE customer_id =
                                        (SELECT id FROM customer WHERE email = 'resort@gmail.com')),
                                        (SELECT id FROM product WHERE name = 'Apple'), 5);
