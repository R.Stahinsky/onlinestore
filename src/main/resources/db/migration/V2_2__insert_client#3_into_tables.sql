INSERT INTO customer (first_name, last_name, email, address, gender,
                                                    birth_date, created_at, updated_at, deleted_at)
VALUES ('Dan', 'Simpson', 'bart@gmail.com', 'Minskaya str.2', 'MALE','1987-01-08',
                                                    '2000-11-08', '2000-11-08', '2000-11-08');

INSERT INTO customer_order (order_number, customer_id, date, price,
                                               address, status, created_at, updated_at, deleted_at)
VALUES (3, (SELECT id FROM customer WHERE email = 'bart@gmail.com'),
        '2020-07-04', 12.4, 'Minskaya str.', 'PROCESSING', '2000-11-08', '2000-11-08', NULL);

INSERT INTO product (name, price, description, product_category_id, created_at, updated_at, deleted_at)
VALUES ('Pineapple', 2.4, 'Sweet', (SELECT id FROM product_category WHERE name = 'Fruits'),
                                                          '2000-11-08', '2000-11-08', '2000-11-08');

INSERT INTO order_details (order_id, product_id, count)
VALUES ((SELECT id FROM customer_order WHERE customer_id =
                                        (SELECT id FROM customer WHERE email = 'bart@gmail.com')),
                                        (SELECT id FROM product WHERE name = 'Pineapple'), 1);
