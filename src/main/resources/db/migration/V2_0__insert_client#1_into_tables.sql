INSERT INTO customer (first_name, last_name, email, address, gender,
                                                    birth_date, created_at, updated_at, deleted_at)
VALUES ('Bob', 'Bobson', 'hilton@gmail.com', 'Minskaya str.', 'MALE','1987-01-08',
                                                    '2000-11-08', '2000-11-08', NULL);

INSERT INTO customer_order (order_number, customer_id, date, price,
                                               address, status, created_at, updated_at, deleted_at)
VALUES (1, (SELECT id FROM customer WHERE email = 'hilton@gmail.com'),
        '2021-08-25', 12.4, 'Minskaya str.', 'COMPLETED', '2000-11-08', '2000-11-08', NULL);

INSERT INTO product (name, price, description, product_category_id, created_at, updated_at, deleted_at)
VALUES ('Banana', 2.4, 'Sweet', (SELECT id FROM product_category WHERE name = 'Fruits'),
                                                          'Sat, 29 Sep 2018 20:49:02 GMT', '2000-11-08', '2000-11-08');

INSERT INTO order_details (order_id, product_id, count)
VALUES ((SELECT id FROM customer_order WHERE customer_id =
                                        (SELECT id FROM customer WHERE email = 'hilton@gmail.com')),
                                        (SELECT id FROM product WHERE name = 'Banana'), 1);
