INSERT INTO customer (first_name, last_name, email, address, gender,
                                                    birth_date, created_at, updated_at, deleted_at)
VALUES ('Don', 'Sanchez', 'sanchez@gmail.com', 'Minskaya str.9', 'MALE','1987-01-08',
                                                    '2000-11-08', '2000-11-08', '2000-11-08');

INSERT INTO customer_order (order_number, customer_id, date, price,
                                               address, status, created_at, updated_at, deleted_at)
VALUES (10, (SELECT id FROM customer WHERE email = 'sanchez@gmail.com'),
        '2020-07-04', 12.4, 'Minskaya str.', 'PROCESSING', '2000-11-08', '2000-11-08', '2000-11-08');

INSERT INTO product (name, price, description, product_category_id, created_at, updated_at, deleted_at)
VALUES ('Boots', 2.4, 'Tasty', (SELECT id FROM product_category WHERE name = 'Shoes'),
                                                          '2021-11-08', '2000-11-08', '2000-11-08');

INSERT INTO order_details (order_id, product_id, count)
VALUES ((SELECT id FROM customer_order WHERE customer_id =
                                        (SELECT id FROM customer WHERE email = 'sanchez@gmail.com')),
                                        (SELECT id FROM product WHERE name = 'Boots'), 1);
