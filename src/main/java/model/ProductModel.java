package model;

import java.time.Instant;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductModel {

  private UUID id;
  private String name;
  private String description;
  private double price;
  private ProductCategoryModel productCategory;
  private Instant createdAt;
  private Instant updatedAt;
  private Instant deletedAt;
  private int count;
}
