package model;

import enums.OrderStatus;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderModel {

  private UUID id;
  private int orderNumber;
  private UUID customerId;
  private Instant date;
  private String address;
  private OrderStatus status;
  private Instant createdAt;
  private Instant updatedAt;
  private Instant deletedAt;
  private List<OrderDetailsModel> orderDetails;
}
