package model;

import entity.OrderEntity;
import enums.Gender;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerModel {

  private UUID id;
  private String firstName;
  private String lastName;
  private String email;
  private String address;
  private Gender gender;
  private Instant birthDate;
  private Instant createdAt;
  private Instant updatedAt;
  private Instant deletedAt;
  private List<OrderEntity> orders;
}
