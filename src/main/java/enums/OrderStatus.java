package enums;

public enum OrderStatus {
  COMPLETED, PROCESSING, ON_HOLD
}
