package util;

import org.flywaydb.core.Flyway;

public class ConnectionUtil {

  private static final String DB_PASSWORD = "root";
  private static final String DB_SCHEMA = "public";
  private static final String DB_URL = "jdbc:postgresql://localhost:5432/onlinestore";
  private static final String DB_USERNAME = "postgres";

  public static void runFlywayScripts() {
    Flyway.configure().schemas(DB_SCHEMA)
        .dataSource(DB_URL, DB_USERNAME, DB_PASSWORD)
        .load()
        .migrate();
  }
}
