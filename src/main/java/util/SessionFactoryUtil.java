package util;

import lombok.experimental.UtilityClass;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

@UtilityClass
public class SessionFactoryUtil {

  public static Session getSession(){
    return new Configuration().configure().buildSessionFactory().getCurrentSession();
  }
}
