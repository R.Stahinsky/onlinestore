package exceptions;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {

  public EntityNotFoundException(final String name, final UUID id) {
    super(String.format("%s with id: %s not found", name, id));
  }
}
