package exceptions;

import java.util.UUID;

public class EntityNotUpdatedException extends RuntimeException {

  public EntityNotUpdatedException(final String name, final UUID id) {
    super(String.format("%s with id: %s not updated", name, id));
  }
}
