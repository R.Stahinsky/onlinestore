package repository.impl;

import entity.ProductEntity;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.hibernate.Session;
import repository.ProductRepository;
import util.SessionFactoryUtil;

public class ProductRepositoryImpl implements ProductRepository {

  private static final String FIND_ALL_BEFORE_DATE_QUERY =
      "SELECT * FROM product WHERE created_at < :date AND deleted_at IS NULL";
  private static final String FIND_ALL_QUERY = "SELECT * FROM product WHERE deleted_at IS NULL";
  private static final String FIND_BY_ID_QUERY =
      "SELECT * FROM product WHERE id = :id AND deleted_at IS NULL";

  @Override
  public Optional<ProductEntity> findById(final UUID id) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final ProductEntity foundProduct = session
        .createNativeQuery(FIND_BY_ID_QUERY, ProductEntity.class)
        .setParameter("id", id)
        .getSingleResult();

    session.getTransaction().commit();
    session.close();
    return Optional.of(foundProduct);
  }

  @Override
  public List<ProductEntity> findAll() {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final List<ProductEntity> foundProducts = session
        .createNativeQuery(FIND_ALL_QUERY, ProductEntity.class)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundProducts;
  }

  @Override
  public UUID create(final ProductEntity product) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final UUID savedProductId = (UUID) session.save(product);

    session.getTransaction().commit();
    session.close();
    return savedProductId;
  }

  @Override
  public Optional<ProductEntity> update(final ProductEntity product) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final ProductEntity updatedProduct = (ProductEntity) session.merge(product);

    session.getTransaction().commit();
    session.close();
    return Optional.of(updatedProduct);
  }

  @Override
  public void delete(final ProductEntity productForDelete) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    session.update(productForDelete);

    session.getTransaction().commit();
    session.close();
  }

  @Override
  public List<ProductEntity> findAllBeforeDate(final Instant date) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final List<ProductEntity> foundProducts = session
        .createNativeQuery(FIND_ALL_BEFORE_DATE_QUERY, ProductEntity.class)
        .setParameter("date", date)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundProducts;
  }

  @Override
  public void deleteAll() {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    session.createNativeQuery("DELETE FROM product CASCADE").executeUpdate();

    session.getTransaction().commit();
    session.close();
  }
}
