package repository.impl;

import entity.ProductCategoryEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.hibernate.Session;
import repository.ProductCategoryRepository;
import util.SessionFactoryUtil;

public class ProductCategoryRepositoryImpl implements ProductCategoryRepository {

  private static final String FIND_ALL_QUERY = "SELECT * FROM product_category";

  @Override
  public Optional<ProductCategoryEntity> findById(final UUID id) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final ProductCategoryEntity foundProductCategory = session
        .get(ProductCategoryEntity.class, id);

    session.getTransaction().commit();
    session.close();
    return Optional.of(foundProductCategory);
  }

  @Override
  public List<ProductCategoryEntity> findAll() {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final List<ProductCategoryEntity> foundProductCategories = session
        .createNativeQuery(FIND_ALL_QUERY, ProductCategoryEntity.class)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundProductCategories;
  }

  @Override
  public UUID create(final ProductCategoryEntity productCategory) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final UUID savedProductCategoryId =
        (UUID) session.save(productCategory);

    session.getTransaction().commit();
    session.close();
    return savedProductCategoryId;
  }

  @Override
  public Optional<ProductCategoryEntity> update(final ProductCategoryEntity productCategory) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final ProductCategoryEntity updatedProductCategory =
        (ProductCategoryEntity) session.merge(productCategory);

    session.getTransaction().commit();
    session.close();
    return Optional.of(updatedProductCategory);
  }

  @Override
  public void delete(final ProductCategoryEntity productCategoryForDelete) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    session.delete(productCategoryForDelete);

    session.getTransaction().commit();
    session.close();
  }

  @Override
  public void deleteAll() {
      final Session session = SessionFactoryUtil.getSession();
      session.beginTransaction();

      session.createNativeQuery("DELETE FROM product_category CASCADE").executeUpdate();

      session.getTransaction().commit();
      session.close();
  }
}
