package repository.impl;

import entity.CustomerEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import repository.CustomerRepository;
import util.SessionFactoryUtil;

public class CustomerRepositoryImpl implements CustomerRepository {

  private static final String FIND_ALL_QUERY = "SELECT * FROM customer WHERE deleted_at IS NULL";
  private static final String FIND_BY_ID_QUERY = "SELECT *  FROM customer WHERE id = :id AND deleted_at IS NULL";

  @Override
  public Optional<CustomerEntity> findById(final UUID id) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final CustomerEntity foundCustomer = session
        .createNativeQuery(FIND_BY_ID_QUERY, CustomerEntity.class)
        .setParameter("id", id)
        .getSingleResult();

    Hibernate.initialize(foundCustomer.getOrders());

    session.getTransaction().commit();
    session.close();
    return Optional.of(foundCustomer);
  }

  @Override
  public List<CustomerEntity> findAll() {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final List<CustomerEntity> foundCustomers = session
        .createNativeQuery(FIND_ALL_QUERY, CustomerEntity.class)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundCustomers;
  }

  @Override
  public UUID create(final CustomerEntity customer) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final UUID savedCustomerId = (UUID) session.save(customer);

    session.getTransaction().commit();
    session.close();
    return savedCustomerId;
  }

  @Override
  public Optional<CustomerEntity> update(final CustomerEntity customer) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final CustomerEntity updatedCustomer = (CustomerEntity) session.merge(customer);

    session.getTransaction().commit();
    session.close();
    return Optional.of(updatedCustomer);
  }

  @Override
  public void delete(final CustomerEntity customerForDelete) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    session.update(customerForDelete);

    session.getTransaction().commit();
    session.close();
  }

  @Override
  public void deleteAll() {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    session.createNativeQuery("DELETE FROM customer CASCADE").executeUpdate();

    session.getTransaction().commit();
    session.close();
  }
}
