package repository.impl;

import entity.OrderDetailsEntity;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.hibernate.Session;
import repository.OrderDetailsRepository;
import util.SessionFactoryUtil;

public class OrderDetailsRepositoryImpl implements OrderDetailsRepository {

  private static final String COUNT_BY_PRODUCT_ID_QUERY =
      "SELECT count FROM order_details WHERE product_id = :productId";
  private static final String FIND_ALL_BY_ORDER_ID_QUERY =
      "SELECT * FROM order_details WHERE order_id = :orderId";
  private static final String FIND_ALL_BY_PRODUCT_ID_QUERY =
      "SELECT * FROM order_details WHERE product_id = :productId";
  private static final String FIND_ALL_QUERY = "SELECT * FROM order_details";

  @Override
  public Optional<OrderDetailsEntity> findById(final UUID id) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final OrderDetailsEntity foundOrderDetails = session.get(OrderDetailsEntity.class, id);

    session.getTransaction().commit();
    session.close();
    return Optional.of(foundOrderDetails);
  }

  @Override
  public List<OrderDetailsEntity> findAll() {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final List<OrderDetailsEntity> foundOrderDetails = session
        .createNativeQuery(FIND_ALL_QUERY, OrderDetailsEntity.class)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundOrderDetails;
  }

  @Override
  public UUID create(final OrderDetailsEntity orderDetails) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final UUID savedOrderDetailsId = (UUID) session.save(orderDetails);

    session.getTransaction().commit();
    session.close();
    return savedOrderDetailsId;
  }

  @Override
  public Optional<OrderDetailsEntity> update(final OrderDetailsEntity orderDetails) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final OrderDetailsEntity updatedOrderDetails = (OrderDetailsEntity) session.merge(orderDetails);

    session.getTransaction().commit();
    session.close();
    return Optional.of(updatedOrderDetails);
  }

  @Override
  public void delete(final OrderDetailsEntity orderDetailsForDelete) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    session.delete(orderDetailsForDelete);

    session.getTransaction().commit();
    session.close();
  }

  @Override
  public List<OrderDetailsEntity> findAllByOrderId(final UUID orderId) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    List<OrderDetailsEntity> foundOrderDetails = session
        .createNativeQuery(FIND_ALL_BY_ORDER_ID_QUERY, OrderDetailsEntity.class)
        .setParameter("orderId", orderId)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundOrderDetails;
  }

  @Override
  public List<OrderDetailsEntity> findAllByProductId(final UUID productId) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    List<OrderDetailsEntity> foundOrderDetails = session
        .createNativeQuery(FIND_ALL_BY_PRODUCT_ID_QUERY, OrderDetailsEntity.class)
        .setParameter("productId", productId)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundOrderDetails;
  }

  @Override
  public Integer countByProductId(final UUID productId) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final Integer count = ((BigInteger) session
        .createNativeQuery(COUNT_BY_PRODUCT_ID_QUERY)
        .setParameter("productId", productId)
        .getSingleResult())
        .intValue();

    session.getTransaction().commit();
    session.close();
    return count;
  }

  @Override
  public void deleteAll() {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    session.createNativeQuery("DELETE FROM order_details CASCADE").executeUpdate();

    session.getTransaction().commit();
    session.close();
  }
}
