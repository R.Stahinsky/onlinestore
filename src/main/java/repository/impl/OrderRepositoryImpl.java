package repository.impl;

import entity.OrderEntity;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import repository.OrderRepository;
import util.SessionFactoryUtil;

public class OrderRepositoryImpl implements OrderRepository {

  private static final String COUNT_PRICE_BEFORE_DATE_QUERY =
      "SELECT AVG(price) FROM customer_order WHERE deleted_at IS NULL AND date > :date";
  private static final String FIND_ALL_BEFORE_DATE_QUERY =
      "SELECT * FROM customer_order WHERE deleted_at IS NULL AND date > :date";
  private static final String FIND_ALL_BY_CUSTOMER_ID_QUERY =
      "SELECT * FROM customer_order WHERE customer_id = :customer_id AND deleted_at IS NULL";
  private static final String FIND_ALL_QUERY =
      "SELECT * FROM customer_order WHERE deleted_at IS NULL";
  private static final String FIND_BY_ID_QUERY =
      "SELECT * FROM customer_order WHERE id = :id AND deleted_at IS NULL";

  @Override
  public Optional<OrderEntity> findById(final UUID id) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final OrderEntity foundOrder = session
        .createNativeQuery(FIND_BY_ID_QUERY, OrderEntity.class)
        .setParameter("id", id)
        .getSingleResult();

    Hibernate.initialize(foundOrder.getOrderDetails());

    session.getTransaction().commit();
    session.close();
    return Optional.of(foundOrder);
  }

  @Override
  public List<OrderEntity> findAll() {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final List<OrderEntity> foundOrders = session
        .createNativeQuery(FIND_ALL_QUERY, OrderEntity.class)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundOrders;
  }

  @Override
  public UUID create(final OrderEntity order) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final UUID uuid = (UUID) session.save(order);

    session.getTransaction().commit();
    session.close();
    return uuid;
  }

  @Override
  public Optional<OrderEntity> update(final OrderEntity order) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final OrderEntity updatedOrder = (OrderEntity) session.merge(order);

    session.getTransaction().commit();
    session.close();
    return Optional.of(updatedOrder);
  }

  @Override
  public void delete(final OrderEntity orderForDelete) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    session.update(orderForDelete);

    session.getTransaction().commit();
    session.close();
  }

  @Override
  public List<OrderEntity> findAllBeforeDate(final Instant date) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final List<OrderEntity> foundOrders = session
        .createNativeQuery(FIND_ALL_BEFORE_DATE_QUERY, OrderEntity.class)
        .setParameter("date", date)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundOrders;
  }

  @Override
  public Double countAveragePriceBeforeDate(final Instant date) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final Double averagePrice = (Double) session
        .createNativeQuery(COUNT_PRICE_BEFORE_DATE_QUERY)
        .setParameter("date", date)
        .getSingleResult();

    session.getTransaction().commit();
    session.close();
    return averagePrice;
  }

  @Override
  public List<OrderEntity> findAllByCustomerId(final UUID customerId) {
    final Session session = SessionFactoryUtil.getSession();
    session.beginTransaction();

    final List<OrderEntity> foundOrders = session
        .createNativeQuery(FIND_ALL_BY_CUSTOMER_ID_QUERY, OrderEntity.class)
        .setParameter("customer_id", customerId)
        .getResultList();

    session.getTransaction().commit();
    session.close();
    return foundOrders;
  }

  @Override
  public void deleteAll() {
      final Session session = SessionFactoryUtil.getSession();
      session.beginTransaction();

      session.createNativeQuery("DELETE FROM customer_order CASCADE").executeUpdate();

      session.getTransaction().commit();
      session.close();
  }
}
