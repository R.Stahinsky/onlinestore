package repository;

import entity.OrderEntity;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderRepository {
  Optional<OrderEntity> findById(final UUID id);

  List<OrderEntity> findAll();

  UUID create(final OrderEntity order);

  Optional<OrderEntity> update(final OrderEntity order);

  void delete(final OrderEntity orderForDelete);

  List<OrderEntity> findAllBeforeDate(final Instant date);

  Double countAveragePriceBeforeDate(final Instant date);

  List<OrderEntity> findAllByCustomerId(final UUID customerId);

  void deleteAll();
}
