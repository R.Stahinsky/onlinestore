package repository;

import entity.OrderDetailsEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderDetailsRepository {

  Optional<OrderDetailsEntity> findById(final UUID id);

  List<OrderDetailsEntity> findAll();

  UUID create(final OrderDetailsEntity orderDetails);

  Optional<OrderDetailsEntity> update(final OrderDetailsEntity orderDetails);

  void delete(final OrderDetailsEntity orderDetailsForDelete);

  List<OrderDetailsEntity> findAllByOrderId(final UUID orderId);

  List<OrderDetailsEntity> findAllByProductId(final UUID productId);

  Integer countByProductId(final UUID productId);

  void deleteAll();
}
