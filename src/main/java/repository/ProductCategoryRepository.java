package repository;

import entity.ProductCategoryEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductCategoryRepository {
  Optional<ProductCategoryEntity> findById(final UUID id);

  List<ProductCategoryEntity> findAll();

  UUID create(final ProductCategoryEntity productCategory);

  Optional<ProductCategoryEntity> update(final ProductCategoryEntity productCategory);

  void delete(final ProductCategoryEntity productCategoryForDelete);

  void deleteAll();
}
