package repository;

import entity.CustomerEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CustomerRepository {

  Optional<CustomerEntity> findById(final UUID id);

  List<CustomerEntity> findAll();

  UUID create(final CustomerEntity customer);

  Optional<CustomerEntity> update(final CustomerEntity customer);

  void delete(final CustomerEntity customerForDelete);

  void deleteAll();
}
