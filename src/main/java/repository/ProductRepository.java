package repository;

import entity.ProductEntity;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductRepository {

  Optional<ProductEntity> findById(final UUID id);

  List<ProductEntity> findAll();

  UUID create(final ProductEntity product);

  Optional<ProductEntity> update(final ProductEntity product);

  void delete(final ProductEntity productForDelete);

  List<ProductEntity> findAllBeforeDate(final Instant date);

  void deleteAll();
}
