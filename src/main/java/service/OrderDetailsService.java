package service;

import entity.OrderDetailsEntity;
import java.util.List;
import java.util.UUID;
import model.OrderDetailsModel;

public interface OrderDetailsService {

  OrderDetailsEntity getById(final UUID id);

  List<OrderDetailsEntity> getAll();

  List<OrderDetailsEntity> getAllByOrderId(final UUID orderId);

  List<OrderDetailsEntity> getAllByProductId(final UUID productId);

  OrderDetailsEntity create(final OrderDetailsEntity orderDetailsForSave);

  OrderDetailsEntity updateById(final UUID id, final OrderDetailsModel orderDetailsForUpdate);

  void deleteById(final UUID id);
}
