package service;

import entity.CustomerEntity;
import java.util.List;
import java.util.UUID;
import model.CustomerModel;

public interface CustomerService {

  CustomerEntity getById(final UUID id);

  List<CustomerEntity> getAll();

  CustomerEntity create(final CustomerEntity customerForSave);

  CustomerEntity updateById(final UUID id, final CustomerModel customerForUpdate);

  void deleteById(final UUID id);
}
