package service;

import entity.OrderEntity;
import java.util.List;
import java.util.UUID;
import model.OrderModel;

public interface OrderService {

  OrderEntity getById(final UUID id);

  List<OrderEntity> getAll();

  OrderEntity create(final OrderEntity orderForSave);

  OrderEntity updateById(final UUID id, final OrderModel orderForUpdate);

  void deleteById(final UUID id);

  List<OrderEntity> getAllByCustomerId(final UUID customerId);
}
