package service.impl;

import entity.OrderEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import model.OrderModel;
import repository.OrderRepository;
import repository.impl.OrderDetailsRepositoryImpl;
import service.OrderDetailsService;
import service.OrderService;

@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

  private final OrderRepository orderRepository;

  private final OrderDetailsService orderDetailsService = new OrderDetailsServiceImpl(
      new OrderDetailsRepositoryImpl());

  @Override
  public OrderEntity getById(final UUID id) {
    return orderRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(OrderEntity.class.getName(), id));
  }

  @Override
  public List<OrderEntity> getAll() {
    return orderRepository.findAll();
  }

  @Override
  public List<OrderEntity> getAllByCustomerId(final UUID customerId) {
    return orderRepository.findAllByCustomerId(customerId);
  }

  @Override
  public OrderEntity create(final OrderEntity orderForSave) {
    return getById(orderRepository.create(orderForSave));
  }

  @Override
  public OrderEntity updateById(final UUID id, final OrderModel orderForUpdate) {
    final OrderEntity foundOrder = orderRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(OrderEntity.class.getName(), id));

    foundOrder.setAddress(orderForUpdate.getAddress());
    foundOrder.setOrderNumber(orderForUpdate.getOrderNumber());
    foundOrder.setOrderDate(orderForUpdate.getDate());

    return orderRepository.update(foundOrder)
        .orElseThrow(() -> new EntityNotUpdatedException(OrderEntity.class.getName(), id));
  }

  @Override
  public void deleteById(final UUID id) {
    final OrderEntity orderForDelete = getById(id);

    orderDetailsService.deleteById(id);

    orderForDelete.setOrderDate(Instant.now());

    orderRepository.delete(orderForDelete);
  }
}
