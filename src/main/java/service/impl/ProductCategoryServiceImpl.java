package service.impl;

import entity.ProductCategoryEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import model.ProductCategoryModel;
import repository.ProductCategoryRepository;
import service.ProductCategoryService;

@RequiredArgsConstructor
public class ProductCategoryServiceImpl implements ProductCategoryService {

  private final ProductCategoryRepository productCategoryRepository;

  @Override
  public ProductCategoryEntity getById(final UUID id) {
    return productCategoryRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(ProductCategoryEntity.class.getName(), id));
  }

  @Override
  public List<ProductCategoryEntity> getAll() {
    return productCategoryRepository.findAll();
  }

  @Override
  public ProductCategoryEntity create(final ProductCategoryEntity productCategoryForSave) {
    return getById(productCategoryRepository.create(productCategoryForSave));
  }

  @Override
  public ProductCategoryEntity updateById(final UUID id,
      final ProductCategoryModel productCategoryForUpdate) {

    final ProductCategoryEntity foundProductCategory = productCategoryRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(ProductCategoryEntity.class.getName(), id));

    foundProductCategory.setName(productCategoryForUpdate.getName());

    return productCategoryRepository.update(foundProductCategory)
        .orElseThrow(
            () -> new EntityNotUpdatedException(ProductCategoryEntity.class.getName(), id));
  }

  @Override
  public void deleteById(final UUID id) {
    final ProductCategoryEntity productCategoryForDelete = getById(id);

    productCategoryRepository.delete(productCategoryForDelete);
  }
}
