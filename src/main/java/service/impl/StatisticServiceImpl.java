package service.impl;

import static mapper.CustomerMapper.mapToCustomerModel;
import static mapper.OrderMapper.mapToOrderModel;
import static mapper.ProductMapper.mapToProductModel;

import entity.CustomerEntity;
import entity.OrderEntity;
import entity.ProductEntity;
import exceptions.EntityNotFoundException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import model.CustomerModel;
import model.OrderModel;
import model.ProductModel;
import repository.CustomerRepository;
import repository.OrderDetailsRepository;
import repository.OrderRepository;
import repository.ProductRepository;
import service.StatisticService;

@RequiredArgsConstructor
public class StatisticServiceImpl implements StatisticService {

  private final CustomerRepository customerRepository;
  private final OrderRepository orderRepository;
  private final OrderDetailsRepository orderDetailsRepository;
  private final ProductRepository productRepository;

  @Override
  public List<OrderModel> getOrdersWithinLastTwoWeeks() {
    final Instant dateTwoWeeksAgo = ZonedDateTime.now().minusDays(14).toInstant();

    final List<OrderEntity> foundOrders = orderRepository.findAllBeforeDate(dateTwoWeeksAgo);
    return foundOrders.stream().map(orderEntity ->
        mapToOrderModel(orderEntity, orderDetailsRepository.findAllByOrderId(orderEntity.getId())))
        .collect(Collectors.toList());
  }

  @Override
  public CustomerModel getCustomerWithBiggestHistoryLastMonth() {
    final Instant dateMonthAgo = ZonedDateTime.now().minusMonths(1).toInstant();

    final List<OrderEntity> ordersBeforeDate = orderRepository.findAllBeforeDate(dateMonthAgo);

    final Map<UUID, Integer> customersIdsWithCount = ordersBeforeDate.stream()
        .collect(Collectors
            .toMap(orderEntity -> orderEntity.getCustomer().getId(), OrderEntity::getOrderNumber));

    final UUID customerIdWithBiggestHistory =
        customersIdsWithCount.entrySet().stream().max(Map.Entry.comparingByValue())
            .get()
            .getKey();

    return mapToCustomerModel(
        customerRepository.findById(customerIdWithBiggestHistory).orElseThrow(
            () -> new EntityNotFoundException(CustomerEntity.class.getName(),
                customerIdWithBiggestHistory)));
  }

  @Override
  public ProductModel getMostPopularProductBeforeDate(final Instant date) {
    final List<ProductEntity> foundProductsBefore = productRepository.findAllBeforeDate(date);

    final Map<UUID, Integer> productIdsWithCount = foundProductsBefore.stream()
        .collect(Collectors.toMap(ProductEntity::getId,
            o -> orderDetailsRepository.countByProductId(o.getId())));

    final UUID popularProductId = Collections
        .max(productIdsWithCount.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();

    final ProductEntity popularProduct = productRepository.findById(popularProductId)
        .orElseThrow(
            () -> new EntityNotFoundException(ProductEntity.class.getName(), popularProductId));

    return mapToProductModel(popularProduct,
        orderDetailsRepository.countByProductId(popularProductId));
  }

  @Override
  public Integer getCountOfOrdersByProductIdAndDate(final UUID productId, final Instant date) {
    List<ProductEntity> foundProducts = productRepository.findAllBeforeDate(date);

    final Map<UUID, Integer> productIdsWithCount = foundProducts.stream()
        .collect(Collectors
            .toMap(ProductEntity::getId, o -> orderDetailsRepository.countByProductId(o.getId())));

    return productIdsWithCount.values().stream().mapToInt(count -> count).sum();
  }

  @Override
  public Double getAveragePriceInLastMonth() {
    final Instant dateMonthAgo = ZonedDateTime.now().minusMonths(1).toInstant();

    return orderRepository.countAveragePriceBeforeDate(dateMonthAgo);
  }
}
