package service.impl;

import entity.OrderDetailsEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import model.OrderDetailsModel;
import repository.OrderDetailsRepository;
import service.OrderDetailsService;

@RequiredArgsConstructor
public class OrderDetailsServiceImpl implements OrderDetailsService {

  private final OrderDetailsRepository orderDetailsRepository;

  @Override
  public OrderDetailsEntity getById(final UUID id) {
    return orderDetailsRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(OrderDetailsEntity.class.getName(), id));
  }

  @Override
  public List<OrderDetailsEntity> getAll() {
    return orderDetailsRepository.findAll();
  }

  @Override
  public List<OrderDetailsEntity> getAllByOrderId(final UUID orderId) {
    return orderDetailsRepository.findAllByOrderId(orderId);
  }

  @Override
  public List<OrderDetailsEntity> getAllByProductId(final UUID productId) {
    return orderDetailsRepository.findAllByProductId(productId);
  }

  @Override
  public OrderDetailsEntity create(final OrderDetailsEntity orderDetailsForSave) {
    return getById(orderDetailsRepository.create(orderDetailsForSave));
  }

  @Override
  public OrderDetailsEntity updateById(final UUID id,
      final OrderDetailsModel orderDetailsForUpdate) {

    final OrderDetailsEntity foundOrderDetails = orderDetailsRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(OrderDetailsEntity.class.getName(), id));

    foundOrderDetails.setCount(orderDetailsForUpdate.getCount());

    return orderDetailsRepository.update(foundOrderDetails)
        .orElseThrow(() -> new EntityNotUpdatedException(OrderDetailsEntity.class.getName(), id));
  }

  @Override
  public void deleteById(final UUID id) {
    final OrderDetailsEntity orderDetailsForDelete = getById(id);

    orderDetailsRepository.delete(orderDetailsForDelete);
  }
}
