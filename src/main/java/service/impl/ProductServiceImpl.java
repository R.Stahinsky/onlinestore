package service.impl;

import entity.ProductEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import model.ProductModel;
import repository.ProductRepository;
import service.OrderDetailsService;
import service.ProductService;

@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;

  private final OrderDetailsService orderDetailsService;

  @Override
  public ProductEntity getById(final UUID id) {
    return productRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(ProductEntity.class.getName(), id));
  }

  @Override
  public List<ProductEntity> getAll() {
    return productRepository.findAll();
  }

  @Override
  public ProductEntity create(final ProductEntity productForSave) {
    return getById(productRepository.create(productForSave));
  }

  @Override
  public ProductEntity updateById(final UUID id, final ProductModel productForUpdate) {
    final ProductEntity foundProduct = productRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(ProductEntity.class.getName(), id));

    foundProduct.setName(productForUpdate.getName());
    foundProduct.setDescription(productForUpdate.getDescription());
    foundProduct.setPrice(productForUpdate.getPrice());

    return productRepository.update(foundProduct)
        .orElseThrow(() -> new EntityNotUpdatedException(ProductEntity.class.getName(), id));
  }

  @Override
  public void deleteById(final UUID id) {
    final ProductEntity productForDelete = getById(id);
    orderDetailsService.getAllByProductId(id)
        .forEach(orderDetails -> orderDetailsService.deleteById(orderDetails.getId()));

    productForDelete.setDeletedAt(Instant.now());

    productRepository.delete(productForDelete);
  }
}
