package service.impl;

import entity.CustomerEntity;
import exceptions.EntityNotFoundException;
import exceptions.EntityNotUpdatedException;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import model.CustomerModel;
import repository.CustomerRepository;
import service.CustomerService;
import service.OrderService;

@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

  private final CustomerRepository customerRepository;

  private final OrderService orderService;

  @Override
  public CustomerEntity getById(final UUID id) {
    return customerRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(CustomerEntity.class.getName(), id));
  }

  @Override
  public List<CustomerEntity> getAll() {
    return customerRepository.findAll();
  }

  @Override
  public CustomerEntity create(final CustomerEntity customerForSave) {
    return getById(customerRepository.create(customerForSave));
  }

  @Override
  public CustomerEntity updateById(final UUID id, final CustomerModel customerForUpdate) {
    final CustomerEntity foundCustomer = customerRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException(CustomerEntity.class.getName(), id));

    foundCustomer.setFirstName(customerForUpdate.getFirstName());
    foundCustomer.setLastName(customerForUpdate.getLastName());
    foundCustomer.setEmail(customerForUpdate.getEmail());
    foundCustomer.setAddress(customerForUpdate.getAddress());
    foundCustomer.setGender(customerForUpdate.getGender());
    foundCustomer.setBirthDate(customerForUpdate.getBirthDate());

    return customerRepository.update(foundCustomer)
        .orElseThrow(() -> new EntityNotUpdatedException(CustomerEntity.class.getName(), id));
  }

  @Override
  public void deleteById(final UUID id) {
    final CustomerEntity customerForDelete = getById(id);

    orderService.getAllByCustomerId(id)
        .forEach(orderEntity -> orderService.deleteById(orderEntity.getId()));

    customerForDelete.setDeletedAt(Instant.now());

    customerRepository.delete(customerForDelete);
  }
}
