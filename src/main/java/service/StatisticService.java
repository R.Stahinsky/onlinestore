package service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import model.CustomerModel;
import model.OrderModel;
import model.ProductModel;

public interface StatisticService {

  CustomerModel getCustomerWithBiggestHistoryLastMonth();

  ProductModel getMostPopularProductBeforeDate(final Instant date);

  Integer getCountOfOrdersByProductIdAndDate(final UUID productId, final Instant date);

  List<OrderModel> getOrdersWithinLastTwoWeeks();

  Double getAveragePriceInLastMonth();
}
