package service;

import entity.ProductEntity;
import java.util.List;
import java.util.UUID;
import model.ProductModel;

public interface ProductService {

  ProductEntity getById(final UUID id);

  List<ProductEntity> getAll();

  ProductEntity create(final ProductEntity productForSave);

  ProductEntity updateById(final UUID id, final ProductModel productForUpdate);

  void deleteById(final UUID id);
}
