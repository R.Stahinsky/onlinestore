package service;

import entity.ProductCategoryEntity;
import java.util.List;
import java.util.UUID;
import model.ProductCategoryModel;

public interface ProductCategoryService {

  ProductCategoryEntity getById(final UUID id);

  List<ProductCategoryEntity> getAll();

  ProductCategoryEntity create(final ProductCategoryEntity productCategoryForSave);

  ProductCategoryEntity updateById(final UUID id,
      final ProductCategoryModel productCategoryForUpdate);

  void deleteById(final UUID id);
}
