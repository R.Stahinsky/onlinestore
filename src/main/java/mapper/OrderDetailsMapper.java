package mapper;

import entity.OrderDetailsEntity;
import java.util.List;
import java.util.stream.Collectors;
import model.OrderDetailsModel;

public class OrderDetailsMapper {

  public static List<OrderDetailsModel> mapToOrderDetailsModel(
      final List<OrderDetailsEntity> orderDetails) {

    return orderDetails.stream().map(orderDetailsEntity -> OrderDetailsModel.builder()
        .id(orderDetails.stream().findFirst().get().getId())
        .productId(orderDetailsEntity.getProduct().getId())
        .orderId(orderDetails.stream().findFirst().get().getOrder().getId())
        .count(orderDetailsEntity.getCount())
        .build()).collect(Collectors.toList());
  }

  public static OrderDetailsModel mapToOrderDetailsModel(
      final OrderDetailsEntity orderDetails) {

    return OrderDetailsModel.builder()
        .id(orderDetails.getId())
        .productId(orderDetails.getProduct().getId())
        .orderId(orderDetails.getOrder().getId())
        .count(orderDetails.getCount())
        .build();
  }
}
