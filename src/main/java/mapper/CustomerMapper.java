package mapper;

import entity.CustomerEntity;
import entity.OrderEntity;
import java.util.stream.Collectors;
import model.CustomerModel;

public class CustomerMapper {

  public static CustomerModel mapToCustomerModel(final CustomerEntity customer) {
    return CustomerModel.builder()
        .id(customer.getId())
        .firstName(customer.getFirstName())
        .lastName(customer.getLastName())
        .email(customer.getEmail())
        .address(customer.getAddress())
        .gender(customer.getGender())
        .birthDate(customer.getBirthDate())
        .createdAt(customer.getCreatedAt())
        .updatedAt(customer.getUpdatedAt())
        .deletedAt(customer.getDeletedAt())
        .orders(customer.getOrders())
        .build();
  }
}
