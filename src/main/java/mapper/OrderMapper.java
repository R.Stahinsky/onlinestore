package mapper;

import static mapper.OrderDetailsMapper.mapToOrderDetailsModel;

import entity.OrderDetailsEntity;
import entity.OrderEntity;
import java.util.List;
import model.OrderModel;

public class OrderMapper {

  public static OrderModel mapToOrderModel(final OrderEntity order, final
  List<OrderDetailsEntity> orderDetails) {

    return OrderModel.builder()
        .id(order.getId())
        .orderNumber(order.getOrderNumber())
        .customerId(order.getCustomer().getId())
        .date(order.getOrderDate())
        .address(order.getAddress())
        .createdAt(order.getCreatedAt())
        .updatedAt(order.getUpdatedAt())
        .deletedAt(order.getDeletedAt())
        .orderDetails(mapToOrderDetailsModel(orderDetails))
        .status(order.getStatus())
        .build();
  }

  public static OrderModel mapToOrderModel(final OrderEntity order,
      final OrderDetailsEntity orderDetails) {
    return OrderModel.builder()
        .id(order.getId())
        .orderNumber(order.getOrderNumber())
        .customerId(order.getCustomer().getId())
        .date(order.getOrderDate())
        .address(order.getAddress())
        .createdAt(order.getCreatedAt())
        .updatedAt(order.getUpdatedAt())
        .deletedAt(order.getDeletedAt())
        .orderDetails(List.of(mapToOrderDetailsModel(orderDetails)))
        .status(order.getStatus())
        .build();
  }
}
