package mapper;

import entity.ProductCategoryEntity;
import model.ProductCategoryModel;

public class ProductCategory {

  public static ProductCategoryModel mapToProductCategoryModel(
      final ProductCategoryEntity productCategory) {

    return ProductCategoryModel.builder()
        .id(productCategory.getId())
        .name(productCategory.getName())
        .build();
  }
}
