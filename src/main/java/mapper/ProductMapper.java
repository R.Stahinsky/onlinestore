package mapper;

import static mapper.ProductCategory.mapToProductCategoryModel;

import entity.ProductEntity;
import model.ProductModel;

public class ProductMapper {

  public static ProductModel mapToProductModel(final ProductEntity product, final Integer count) {
    return ProductModel.builder()
        .id(product.getId())
        .name(product.getName())
        .description(product.getDescription())
        .price(product.getPrice())
        .productCategory(mapToProductCategoryModel(product.getProductCategory()))
        .createdAt(product.getCreatedAt())
        .updatedAt(product.getUpdatedAt())
        .deletedAt(product.getDeletedAt())
        .count(count)
        .build();
  }
}
