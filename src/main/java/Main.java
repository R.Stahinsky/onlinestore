import static util.ConnectionUtil.runFlywayScripts;

public class Main {

  public static void main(String[] args) {
    runFlywayScripts();
  }
}
